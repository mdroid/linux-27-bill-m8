/*
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html

 * (C) Copyright 2006 Marvell International Ltd.
 * All Rights Reserved
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/hwmon-sysfs.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <asm/setup.h>
#include <asm/mach/arch.h>
#include <asm/mach/irq.h>

#include <mach/hardware.h>
#include <asm/io.h>
#include <asm/irq.h>
//#include <mach/pxa-regs.h>
//#include <mach/mfp-pxa300.h>
#include <mach/gpio.h>


static struct synaptics510_ts {
	struct input_dev *input_dev;
	s16 xp[2];
	s16 yp[2];
	char count;
	int shift;
	char phys[32];
}ts;



static int synaptics510_ts_detect(struct i2c_client *client, int kind,struct i2c_board_info *info);
static int synaptics510_ts_probe(struct i2c_client *client, const struct i2c_device_id *id);



static struct i2c_client * synaptics510_ts_client = NULL;


/* Addresses to scan */
static unsigned short 			bk_normal_i2c[] 	=	{ 0x40,I2C_CLIENT_END};
static unsigned short 			bk_probe[2]			= 	{ I2C_CLIENT_END, I2C_CLIENT_END };
static unsigned short 			bk_ignore[2]		=	{ I2C_CLIENT_END, I2C_CLIENT_END };
//static unsigned short 			bk_force[2]			=	{ I2C_CLIENT_END, I2C_CLIENT_END };


static struct i2c_client_address_data addr_data_ = 
{
	.normal_i2c 		= bk_normal_i2c,
	.probe 				= bk_probe,
	.ignore 			= bk_ignore,
	.forces				= NULL,
};

/*
static int synaptics510_ts_attach_adapter(struct i2c_adapter *adapter)
{
	printk(KERN_INFO"synaptics510_ts_attach_adapter\n");
	return i2c_probe(adapter, &addr_data, synaptics510_ts_detect);
}
*/
static int synaptics510_ts_remove(struct i2c_client *client)
{
	int ret;
	int irq;
	
	irq = IRQ_GPIO(118);
	
	//printk(KERN_INFO"irq:%d",irq);
	
	free_irq(irq,synaptics510_ts_client);

	input_unregister_device(ts.input_dev);

	if ((ret = i2c_detach_client(synaptics510_ts_client)))
		return ret;

	kfree(i2c_get_clientdata(synaptics510_ts_client));
	
	return 0;
}

static const struct i2c_device_id synaptics510_ts_id[] = {
	{ "synaptics-ts", 0 },
	{ }
};

//synaptics510_ts
static struct i2c_driver synaptics510_ts_driver = 
{
	.driver = 
	{
		.owner	= THIS_MODULE,
		.name	= "synaptics-ts",
	},
	.probe		= synaptics510_ts_probe,
	.remove		= synaptics510_ts_remove,
	.id_table	= synaptics510_ts_id,

	.detect		= synaptics510_ts_detect,
	.address_data	= &addr_data_,
};


static char counth=0;

static irqreturn_t stylus_action(int irq, void *dev_id)
{

	int ret;
	char buf[12] = {0x00};
	//int i;
	
	//printk(KERN_INFO"stulus_action,irq=%d\n",irq);

	disable_irq(irq);


	ret = i2c_master_recv(synaptics510_ts_client,buf,12);
	if(ret!=12)
	printk(KERN_ALERT"ret = %d\n",ret);

	
	ts.count = buf[0];
 	//printk(KERN_INFO"ts.count=%d\n",ts.count);	
	if (ts.count != 0) 
	{
		if(ts.count == 1)
		{
			//__s16 temp;
			counth = 0;

			ts.xp[0] = buf[3]*0x100+buf[2];
			ts.yp[0] = buf[5]*0x100+buf[4];
			
		
			//printk(KERN_ALERT"-------------ts.count = 1-----------------YDN\n");

			//printk(KERN_ALERT"---------------ts.x = %d, ts.y = %d\n",ts.xp[0],ts.yp[0]);
		if(ts.xp[0]>0xe00) //5120
			ts.xp[0] = 0xe00;
		else if(ts.xp < 0) //924
			ts.xp[0] = 0;
		
		if(ts.yp[0]>0x199c)
			ts.yp[0] = 0x199c;
		else if(ts.yp[0] < 0)	
			ts.yp[0] = 0;
		ts.xp[0]= 0xe00 -ts.xp[0];
		ts.yp[0]= 0x199c -ts.yp[0];			
		//calibrate		
		ts.xp[0]= (ts.xp[0]* 34)>>8;
		ts.yp[0]= (ts.yp[0]* 31)>>8;		
		
		/*	
			if(ts.xp[0]>0xe00) //3584
				ts.xp[0] = 0xe00;
			else if(ts.xp < 0) 
				ts.xp[0] = 0;
			if(ts.yp[0]>0x199c)
				ts.yp[0] = 0x199c;
			else if(ts.yp[0] < 0)
		  		ts.yp[0] = 0;
		  
		  ts.xp[0]= 0xe00 -ts.xp[0];
		  ts.yp[0]= 0x199c -ts.yp[0];
		  //calibrate		
		  ts.xp[0]= (ts.xp[0]* 34)>>8;
		  ts.yp[0]= (ts.yp[0]* 31)>>8;
		  */
		  /*
		  temp = ts.xp[0];
		  ts.xp[0] = ts.yp[0];
		  ts.yp[0] = temp;*/
		  printk(KERN_ALERT"calibrate-----------ts.x = %d, ts.y = %d\n",ts.xp[0],ts.yp[0]);		
			input_report_abs(ts.input_dev, ABS_X, ts.xp[0] );
			input_report_abs(ts.input_dev, ABS_Y, ts.yp[0] );
			input_report_abs(ts.input_dev, ABS_PRESSURE, 1);
			input_report_key(ts.input_dev, BTN_TOUCH, 1);
		}
		/*
		else if(ts.count == 2)
		{	
		 //滚动事件
		 //如果滚轮事件不经过tslib，我要自己处理坐标值
		 //x*320/5120  y*240/8192
			
			//printk(KERN_ALERT"----------------------------------------------------tt--\n");

			ts.xp[0] = buf[3]*0x100+buf[2];
			ts.yp[0] = buf[5]*0x100+buf[4];
			ts.xp[1] = buf[7]*0x100+buf[6];
		  	ts.yp[1] = buf[9]*0x100+buf[8];

			if(ts.xp[0] > 0x1400)
				ts.xp[0] = 0x1400;
			else if(ts.xp[0] < 0)
				ts.xp[0] = 0;
		  
			if(ts.yp[0] > 0x2000)
				ts.yp[0] = 0x2000;
			else if(ts.yp[0] < 0)
		  		ts.yp[0] = 0;

			if(ts.xp[1] > 0x1400)
		  		ts.xp[1] = 0x1400;
		  	else if(ts.xp[1] < 0)
		  		ts.xp[1] = 0;

			if(ts.yp[1] > 0x2000)
		  		ts.yp[1] = 0x2000;
		  	else if(ts.yp[1] < 0)
				ts.yp[1] = 0;

			dx = abs(ts.xp[1]-ts.xp[0]);
			dy = abs(ts.yp[1]-ts.yp[0]);

			old_count = buf[1];

			//printk(KERN_INFO"old_count=%d-ts.xp[0]=%d,,ts.yp[0]=%d,ts.xp[1]=%d,ts.yp[1]=%d,dx=%d,dy=%d\n",old_count,ts.xp[0],ts.yp[0],ts.xp[1],ts.yp[1],dx,dy);			
			
			counth++;
			if(counth==1)
			{
				ydx=dx;
				ydy=dy; 
				ydz=int_sqrt(dx*dx+dy*dy);
				//printk(KERN_INFO"ydz=%d\n",ydz);
			}
			else
			{
				int wheel;
				
				dz = int_sqrt(dx*dx+dy*dy);
				if(dz-ydz>0)
					wheel = 4;
				else if(dz - ydz < 0)
					wheel = 5;
				else
					wheel = 0;
				
				//printk(KERN_INFO"wheel=%d\n",wheel);
				if(wheel)
				{
					input_report_rel(ts.input_dev, REL_WHEEL, (wheel*0x10000) | abs(dz-ydz));
				}

				printk(KERN_INFO"ydz=%d,dz=%d,abs(dz-ydz)=%d\n",ydz,dz,abs(dz-ydz));
				ydz = dz;

			}
		  }
		  */
		  else 
		  {
	//	  	printk(KERN_INFO"ts-count=%d\n",ts.count);
		  	//ts.count = 0;
				enable_irq(irq);
				return IRQ_HANDLED;
		  }
	}
	else
	{
		//printk(KERN_ALERT"---------------ts.count = 0 -------------ydn\n");
		counth = 0;
		//dn_now = 0;

		input_report_abs(ts.input_dev, ABS_PRESSURE, 0);
		input_report_key(ts.input_dev, BTN_TOUCH, 0);
		//input_report_key(ts.input_dev, BTN_TOUCH, 0);
		//input_report_abs(ts.input_dev, ABS_PRESSURE, 0);
		//input_report_key(ts.input_dev, BTN_MIDDLE, 0);
		//input_sync(ts.input_dev);
	}
 	
 	input_sync(ts.input_dev);

	enable_irq(irq);

	return IRQ_HANDLED;
}

/* Return 0 if detection is successful, -ENODEV otherwise */
static int synaptics510_ts_detect(struct i2c_client *client, int kind,struct i2c_board_info *info)
{
	struct i2c_adapter *adapter = client->adapter;

	printk(KERN_INFO"synaptics510_ts_detect\n");

	if (!i2c_check_functionality(adapter, I2C_FUNC_I2C))
		return -ENODEV;


	/* This is the place to detect whether the chip at the specified
	   address really is a synaptics chip. However, there is no method known
	   to detect whether an I2C chip is a synaptics or any other I2C chip. */
	strlcpy(info->type, "synaptics-ts", I2C_NAME_SIZE);

	return 0;
}

#define MFPR0_GPIO120  0x40E10654
#define MFPR0_GPIO118  0x40E1064c
extern void pxa3xx_mfp_config(mfp_cfg_t *mfp_cfgs, int num);

static int synaptics510_ts_probe(struct i2c_client *client, const struct i2c_device_id *id)
{

	//struct i2c_client *new_client;
	//struct synaptics510_ts_data *data;
	int irq;
	int ret = 0;
	char dn_synaptics510_ts_start_read_addr[2] ;
	//char dn_synaptics510_ts_read_data[11] = {0};
	//int i;int a;
	//int mfp_reg;
	unsigned int temp;
	void __iomem *p;

	printk(KERN_INFO"synaptics510_ts_probe\n");

	
	synaptics510_ts_client = kmalloc(sizeof(struct i2c_client), GFP_KERNEL );

	if (!synaptics510_ts_client) {
		ret = -ENOMEM;
		goto exit;
	}

	memset(&ts, 0, sizeof(struct synaptics510_ts));

	ts.input_dev = input_allocate_device();
	if(!ts.input_dev)
	{
		input_free_device(ts.input_dev);
		goto exit_kfree;
	}
	
	//ts.input_dev->private = &ts;
	ts.input_dev->name = "synaptics-ts";
	ts.input_dev->phys = "ts";
	ts.input_dev->dev.parent = &client->adapter->dev;
	
	ts.input_dev->open = NULL;
	ts.input_dev->close = NULL;
	
	
	__set_bit(EV_ABS, ts.input_dev->evbit);
	__set_bit(ABS_X, ts.input_dev->absbit);
	__set_bit(ABS_Y, ts.input_dev->absbit);
	__set_bit(ABS_PRESSURE, ts.input_dev->absbit);

	//add android event	
	__set_bit(BTN_TOUCH, ts.input_dev->keybit);
	__set_bit(EV_SYN, ts.input_dev->evbit);
	__set_bit(EV_KEY, ts.input_dev->evbit);
			  	

	memset(synaptics510_ts_client, 0, sizeof(struct i2c_client));

	synaptics510_ts_client->addr = 0x40;
	synaptics510_ts_client->adapter = client->adapter;
	synaptics510_ts_client->driver = &synaptics510_ts_driver;
	synaptics510_ts_client->flags = 0;

	mdelay(100);

	dn_synaptics510_ts_start_read_addr[0] = 0x12;
	dn_synaptics510_ts_start_read_addr[1] = 0xaa;
	ret = i2c_master_send(synaptics510_ts_client,dn_synaptics510_ts_start_read_addr,2);

	dn_synaptics510_ts_start_read_addr[0] = 0x11;
	dn_synaptics510_ts_start_read_addr[1] = 0x04;
	ret = i2c_master_send(synaptics510_ts_client,dn_synaptics510_ts_start_read_addr,2);


	mdelay(300);

	do{
		if(gpio_get_value(118)==0)
		{
			printk(KERN_ALERT".....\n");
			ret = 0;
			
		}/*
		else
		{
			ret = 1;
			
		}			
		*/																			}while(ret);


	dn_synaptics510_ts_start_read_addr[0] = 0x11;
	dn_synaptics510_ts_start_read_addr[1] = 0x03;
	ret = i2c_master_send(synaptics510_ts_client,dn_synaptics510_ts_start_read_addr,2);
	
	printk(KERN_ALERT"ret = %d\n",ret);

	/*
	gpio_direction_output(120,0);
	mdelay(100);
	ret = gpio_get_value(120);
	printk(KERN_INFO"gpio%d=0,%d\n",120,ret);
	mdelay(100);
	gpio_set_value(120,1);
	mdelay(100);
	ret = gpio_get_value(120);
	printk(KERN_INFO"gpio%d=1,%d\n",120,ret);
	*/
	gpio_direction_input(118);
	//gpio_free(120);

	//利用接口无法控制MFPR 手动控制 设置成GPIO口
	//__raw_writel(0xc1d0,p);//下降沿触发
	
	
	//temp = __raw_readl(p);	
	
	irq = gpio_to_irq(118);
	if(irq<0)
	printk(KERN_INFO"gpio irq:%d\n",irq);
  

	ret = request_irq(irq, stylus_action, IRQF_DISABLED | IRQT_FALLING, "synaptics-ts", synaptics510_ts_client);
	
	if (ret)
	{	
		printk(KERN_ALERT"request_irq IRQ_GPIO_118 fail %d\n",ret);
		goto exit_kfree;
	}
	//printk(KERN_INFO"******************2*********\n");

	strlcpy(synaptics510_ts_client->name, "synaptics-ts", I2C_NAME_SIZE);

	ret = input_register_device(ts.input_dev);
	
	printk(KERN_INFO"register-device:%d\n",ret);

	return 0;
	
exit_kfree:
	kfree(synaptics510_ts_client);
exit:
	
	return ret;
}


static int __init synaptics510_ts_init(void)
{
	printk(KERN_INFO"synaptics510_ts_init\n");
	return i2c_add_driver(&synaptics510_ts_driver);
}

static void __exit synaptics510_ts_exit(void)
{
	printk(KERN_INFO"synaptics510_ts_exit\n");
	i2c_del_driver(&synaptics510_ts_driver);
}

MODULE_AUTHOR("liuyihui<liuyihui@meizutel.com.cn>");
MODULE_DESCRIPTION(" synaptics touchscreen,i2c interface");
MODULE_LICENSE("GPL");

module_init(synaptics510_ts_init);
module_exit(synaptics510_ts_exit);

