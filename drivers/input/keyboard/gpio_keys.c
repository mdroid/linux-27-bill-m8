/*
 * Driver for keys on GPIO lines capable of generating interrupts.
 *
 * Copyright 2005 Phil Blundell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/gpio_keys.h>

#include <asm/gpio.h>
#include <plat/gpio-cfg.h>


#ifdef DEBUG
#define dprintk(msg...) printk(KERN_DEBUG "m8_buttons: " msg);
#else
#define dprintk(msg...)
#endif

#define M8_KEY_DEBUG
#ifdef  M8_KEY_DEBUG
#define	m8_key_dbg(fmt, arg...)    printk(KERN_INFO "m8_key: %s(line %d): " fmt "\n", 	\
	__FUNCTION__, __LINE__, ##arg)
#else
#define	m8_key_dbg(fmt, arg...)	do {} while (0)
#endif

struct gpio_button_data {
	struct gpio_keys_button *button;
	struct input_dev *input;
	struct timer_list timer;
};

struct gpio_keys_drvdata {
	struct input_dev *input;
	struct gpio_button_data data[0];
};

extern void meizu_m8_setup_keypad_cfg_gpio();//add by liuyihui 09-08-31
extern int s3c_gpio_cfgpin(unsigned int pin, unsigned int config);//add by liuyihui 09-08-31

static void gpio_keys_report_event(struct gpio_keys_button *button,
				   struct input_dev *input)
{
	unsigned int type = button->type ?button->type: EV_KEY;
	int state = (gpio_get_value(button->gpio) ? 1 : 0) ^ button->active_low;

	m8_key_dbg("Key state == %d...\n",state);//add by hui for test

	input_event(input, type, button->code, !state);
	input_sync(input);
}

static void gpio_check_button(unsigned long _data)
{
	struct gpio_button_data *data = (struct gpio_button_data *)_data;

	gpio_keys_report_event(data->button, data->input);
}

static irqreturn_t gpio_keys_isr(int irq, void *dev_id)
{
	struct platform_device *pdev = dev_id;
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	struct gpio_keys_drvdata *ddata = platform_get_drvdata(pdev);
	int i;
	
unsigned long flags;
int down;

//local_irq_save(flags);

	for (i = 0; i < pdata->nbuttons; i++) {
		struct gpio_keys_button *button = &pdata->buttons[i];

		if (irq == gpio_to_irq(button->gpio)) {
			struct gpio_button_data *bdata = &ddata->data[i];

/*add by cefanty 2009-08-21*/
/*
*用于保证按一次只有一个中断
*/
//	if (!button)
//		return IRQ_HANDLED;
	
	s3c_gpio_cfgpin(button->gpio,S3C_GPIO_INPUT);
	down = !(gpio_get_value(button->gpio));
	s3c_gpio_cfgpin(button->gpio,S3C_GPIO_SFN(2));
	
	/* the power button of the ipaq are tricky. They send 'released' events even
	 * when the button are already released. The work-around is to proceed only 
	 * if the state changed.
	 **/
	 
	if (button->wakeup == down)
		return IRQ_HANDLED;
	
	button->wakeup = down;
	
	//dprintk("%s button %s\n",button->desc, down ? "pressed" : "released");
	//m8_key_dbg("%s button %s\n",button->desc, down ? "pressed" : "released");
/*add end*/	

			if (button->debounce_interval)
				mod_timer(&bdata->timer,
					  jiffies +
					  msecs_to_jiffies(button->debounce_interval));
			else{
				//if(0 == down){//add by liuyihui 09-09-03
					gpio_keys_report_event(button, bdata->input);
					m8_key_dbg("Keycode==%d...\n",button->code);
					}
				//}

			return IRQ_HANDLED;
		}
	}

//local_irq_restore(flags);

	return IRQ_NONE;
}

static int __devinit gpio_keys_probe(struct platform_device *pdev)
{
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	struct gpio_keys_drvdata *ddata;
	struct input_dev *input;
	int i, error;
	int wakeup = 0;

	ddata = kzalloc(sizeof(struct gpio_keys_drvdata) +
			pdata->nbuttons * sizeof(struct gpio_button_data),
			GFP_KERNEL);
	input = input_allocate_device();
	if (!ddata || !input) {
		error = -ENOMEM;
		goto fail1;
	}

	platform_set_drvdata(pdev, ddata);

	input->name = pdev->name;
	input->phys = "gpio-keys/input0";
	input->dev.parent = &pdev->dev;

	input->id.bustype = BUS_HOST;
	input->id.vendor = 0x0001;
	input->id.product = 0x0001;
	input->id.version = 0x0100;

	ddata->input = input;

	meizu_m8_setup_keypad_cfg_gpio();//add by liuyihui 09-08-31
	
	for (i = 0; i < pdata->nbuttons; i++) {
		struct gpio_keys_button *button = &pdata->buttons[i];
		struct gpio_button_data *bdata = &ddata->data[i];
		int irq;
		unsigned int type = button->type ?: EV_KEY;

		bdata->input = input;
		bdata->button = button;
		setup_timer(&bdata->timer,gpio_check_button, (unsigned long)bdata);

		irq = gpio_to_irq(button->gpio);

		if (irq < 0) {
			error = irq;
			pr_err("gpio-keys: Unable to get irq number"
				" for GPIO %d, error %d\n",
				button->gpio, error);
			gpio_free(button->gpio);
			goto fail2;
		}

		error = request_irq(irq, gpio_keys_isr,
				    IRQF_SAMPLE_RANDOM | 
				    	IRQF_TRIGGER_RISING |
					IRQF_TRIGGER_FALLING
					//IRQF_TRIGGER_LOW
					,
				    button->desc ? button->desc : "gpio_keys",
				    pdev);
		if (error) {
			pr_err("gpio-keys: Unable to claim irq %d; error %d\n",
				irq, error);
			gpio_free(button->gpio);
			goto fail2;
		}

		if (button->wakeup)
			wakeup = 1;

		input_set_capability(input, type, button->code);

		//printk("hui test: probe End...[%s %d %s]:\n ",__FILE__,__LINE__,__FUNCTION__);//add by hui test

	}

	error = input_register_device(input);
	if (error) {
		pr_err("gpio-keys: Unable to register input device, "
			"error: %d\n", error);
		goto fail2;
	}

	device_init_wakeup(&pdev->dev, wakeup);

	return 0;

 fail2:
	while (--i >= 0) {
		free_irq(gpio_to_irq(pdata->buttons[i].gpio), pdev);
		if (pdata->buttons[i].debounce_interval)
			del_timer_sync(&ddata->data[i].timer);
		gpio_free(pdata->buttons[i].gpio);
	}

	platform_set_drvdata(pdev, NULL);
 fail1:
	input_free_device(input);
	kfree(ddata);

	return error;
}

static int __devexit gpio_keys_remove(struct platform_device *pdev)
{
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	struct gpio_keys_drvdata *ddata = platform_get_drvdata(pdev);
	struct input_dev *input = ddata->input;
	int i;

	device_init_wakeup(&pdev->dev, 0);

	for (i = 0; i < pdata->nbuttons; i++) {
		int irq = gpio_to_irq(pdata->buttons[i].gpio);
		free_irq(irq, pdev);
		if (pdata->buttons[i].debounce_interval)
			del_timer_sync(&ddata->data[i].timer);
		gpio_free(pdata->buttons[i].gpio);
	}

	input_unregister_device(input);

	return 0;
}


#ifdef CONFIG_PM
static int gpio_keys_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	int i;

	if (device_may_wakeup(&pdev->dev)) {
		for (i = 0; i < pdata->nbuttons; i++) {
			struct gpio_keys_button *button = &pdata->buttons[i];
			if (button->wakeup) {
				int irq = gpio_to_irq(button->gpio);
				enable_irq_wake(irq);
			}
		}
	}

	return 0;
}

static int gpio_keys_resume(struct platform_device *pdev)
{
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	int i;

	if (device_may_wakeup(&pdev->dev)) {
		for (i = 0; i < pdata->nbuttons; i++) {
			struct gpio_keys_button *button = &pdata->buttons[i];
			if (button->wakeup) {
				int irq = gpio_to_irq(button->gpio);
				disable_irq_wake(irq);
			}
		}
	}

	return 0;
}
#else
#define gpio_keys_suspend	NULL
#define gpio_keys_resume	NULL
#endif

static struct platform_driver gpio_keys_device_driver = {
	.probe		= gpio_keys_probe,
	.remove		= __devexit_p(gpio_keys_remove),
	.suspend	= gpio_keys_suspend,
	.resume		= gpio_keys_resume,
	.driver		= {
		.name	= "meizum8-buttons",
		.owner	= THIS_MODULE,
	}
};

static int __init gpio_keys_init(void)
{
	return platform_driver_register(&gpio_keys_device_driver);
}

static void __exit gpio_keys_exit(void)
{
	platform_driver_unregister(&gpio_keys_device_driver);
}

module_init(gpio_keys_init);
module_exit(gpio_keys_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phil Blundell <pb@handhelds.org>");
MODULE_DESCRIPTION("Keyboard driver for CPU GPIOs");
MODULE_ALIAS("platform:gpio-keys");
