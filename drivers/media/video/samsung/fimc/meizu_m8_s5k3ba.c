/* linux/drivers/media/video/samsung/s5k3ba.c
 *
 * Samsung S5K3BA CMOS Image Sensor driver
 *
 * Jinsung Yang, Copyright (c) 2009 Samsung Electronics
 * 	http://www.samsungsemi.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/i2c-id.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/init.h>
#include <asm/io.h>

#include "s3c_fimc.h"
#include "meizu_m8_s5k3ba.h"

#include <linux/delay.h>/*add by liuyihui 2009-09-16*/
#include <mach/gpio.h>/*add by liuyihui 2009-08-24*/
#include <plat/gpio-cfg.h>/*add by liuyihui 2009-08-24*/
#include <plat/regs-gpio.h>/*add by liuyihui 2009-08-24*/

#define M8_CAMERA_DEBUG
#ifdef  M8_CAMERA_DEBUG
#define	m8_camera_dbg(fmt, arg...)    printk(KERN_INFO "m8_camera: %s(line %d): " fmt "\n", 	\
	__FUNCTION__, __LINE__, ##arg)
#else
#define	m8_camera_dbg(fmt, arg...)	do {} while (0)
#endif

#define S5K3BA_I2C_ADDR		0xC0	//modified by liuyihui,	orig:5a

const static u16 ignore[] = { I2C_CLIENT_END };
const static u16 normal_addr[] = { (S5K3BA_I2C_ADDR >> 1), I2C_CLIENT_END };
const static u16 *forces[] = { NULL };
static struct i2c_driver s5k3ba_i2c_driver;

static struct s3c_fimc_camera s5k3ba_data = {
	.id 		= CONFIG_VIDEO_FIMC_CAM_CH,
	.type		= CAM_TYPE_ITU,
	.mode		= ITU_601_YCBCR422_8BIT,
	.order422	= CAM_ORDER422_8BIT_YCRYCB,
	.clockrate	= 48000000,
	.width		= 640,
	.height		= 480,
	.offset		= {
		.h1 = 0,
		.h2 = 0,
		.v1 = 0,
		.v2 = 0,
	},

	.polarity	= {
		.pclk	= 0,
		.vsync	= 0,
		.href	= 0,
		.hsync	= 0,
	},

	.initialized	= 0,	
};

static struct i2c_client_address_data addr_data = {
	.normal_i2c	= normal_addr,
	.probe		= ignore,
	.ignore		= ignore,
	.forces		= forces,
};

//static void __init meizu_m8_s5k3ba_power(int value);
//void meizu_m8_s5k3ba_setup_gpio();

/*add by liuyihui 2009-08-24*/
/*
*配置摄像头GPIO
*/
void meizu_m8_s5k3ba_setup_gpio()
{
	unsigned int gpio;
	unsigned int end;

	end = S3C64XX_GPF(1 + 12);
#if 1
	/* Set all the necessary GPK pins to special-function 0 */
	for (gpio = S3C64XX_GPF(0); gpio < end; gpio++) {
			s3c_gpio_setpull(gpio, S3C_GPIO_PULL_DOWN);//下拉为低电平
			s3c_gpio_cfgpin(gpio, S3C_GPIO_SFN(2));//摄像头数据方式
	}
#endif

}

EXPORT_SYMBOL(meizu_m8_s5k3ba_setup_gpio);
/*add end*/

/*
*拉高GPL4,给camera上电
*拉高GPK15,给camera powerdown
*/
static void __init meizu_m8_s5k3ba_power(int value){

	unsigned int gpio;
	gpio= S3C64XX_GPL(4);
	s3c_gpio_setpull(gpio, S3C_GPIO_PULL_DOWN);	
	s3c_gpio_setpin(gpio, value&0x01);	
	s3c_gpio_cfgpin(gpio, S3C_GPIO_OUTPUT);	
	s3c_gpio_setpin(gpio, value&0x01);

	gpio= S3C64XX_GPK(15);
	s3c_gpio_setpull(gpio, S3C_GPIO_PULL_DOWN);	
	s3c_gpio_setpin(gpio, !value);	
	s3c_gpio_cfgpin(gpio, S3C_GPIO_OUTPUT);	
	s3c_gpio_setpin(gpio, !value);

	gpio= S3C64XX_GPF(3);
	s3c_gpio_setpull(gpio, S3C_GPIO_PULL_DOWN);	
	s3c_gpio_setpin(gpio, !value);	
	s3c_gpio_cfgpin(gpio, S3C_GPIO_OUTPUT);	
	s3c_gpio_setpin(gpio, !value);	
	mdelay(100);
	s3c_gpio_setpull(gpio, S3C_GPIO_PULL_UP);	
	s3c_gpio_setpin(gpio, value);	
	s3c_gpio_cfgpin(gpio, S3C_GPIO_OUTPUT);	
	s3c_gpio_setpin(gpio, value);	
	mdelay(200);

	m8_camera_dbg("\nCamera s5k3ba power %s\n", value?"on":"off");

}
//EXPORT_SYMBOL(meizu_m8_s5k3ba_power);

void Camera_Module_Initialize()
{
	printk("<Camera_Module_Initialize> initial Camera module , download Binary and get version start\n");

	int ret;

	
	// 1. CoreISP3_Initialize, Main ISP3 Initialize Routine
	//CoreISP3_Initialize();

	// 2. preview 
	//CoreISP3_SetAutoWhiteBalance(enISP_FUNC_AWB_ON);///fang
	//CoreISP3_SetAutoExposure(enISP_FUNC_AE_ON);    ///fang  
	//CoreISP3_SetResolution(PrevenISP_RES_1_3MP,CMD_Preview);////set resolution--fang,ok

	//CoreISP3_OutpYCbCr();///Preview output Format---fang

/*add by liuyihui 2009-08-24*/
/*
*打开摄像头电源
*/
	meizu_m8_s5k3ba_setup_gpio();

	meizu_m8_s5k3ba_power(1);

/*add end*/

m8_camera_dbg("s5k3ba camera power on...\n");//add by hui for test


}
/*add end*/

static void s5k3ba_start(struct i2c_client *client)
{
	int i;

	for (i = 0; i < S5K3BA_INIT_REGS16; i++) {
		s3c_fimc_i2c_write_addr16(client, s5k3ba_init_reg16[i].subaddr, s5k3ba_init_reg16[i].value);
		//s3c_fimc_i2c_write(client, s5k3ba_init_reg8[i].subaddr,s5k3ba_init_reg8[i].value);
		
	}
}

static int s5k3ba_attach(struct i2c_adapter *adap, int addr, int kind)
{
	struct i2c_client *c;

//	m8_camera_dbg("...\n");//add by hui for test

	c = kmalloc(sizeof(*c), GFP_KERNEL);
	if (!c)
		return -ENOMEM;

	memset(c, 0, sizeof(struct i2c_client));	

	strcpy(c->name, "s5k3ba");
	c->addr = addr;
	c->adapter = adap;
	c->driver = &s5k3ba_i2c_driver;

	s5k3ba_data.client = c;

	info("s5k3ba attached successfully\n");

	return i2c_attach_client(c);
}

static int s5k3ba_attach_adapter(struct i2c_adapter *adap)
{
	int ret = 0;
/*
*打开摄像头电源
*/
	meizu_m8_s5k3ba_setup_gpio();

	meizu_m8_s5k3ba_power(1);

/*add end*/

m8_camera_dbg("s5k3ba camera power on...\n");//add by hui for test

	s3c_fimc_register_camera(&s5k3ba_data);

//	m8_camera_dbg("...\n");//add by hui for test
	mdelay(200);	//add by hui 09-09-22,延时让摄像头重启初始化成功
	
	ret = i2c_probe(adap, &addr_data, s5k3ba_attach);
	if (ret) {
		err("failed to attach s5k3ba driver\n");
		ret = -ENODEV;
	}

//	m8_camera_dbg("s5k3ba_attach_adapter return...\n");//add by hui for test

	return ret;
}

static int s5k3ba_detach(struct i2c_client *client)
{
m8_camera_dbg("\n\n");

	i2c_detach_client(client);

m8_camera_dbg("\n\n");

/*add by liuyihui 2009-08-24*/
/*
*关闭摄像头电源
*/
	//meizu_m8_s5k3ba_power(0);/*add by liuyihui 2009-09-19*/
/*add end*/
	
	return 0;
}

static int s5k3ba_change_resolution(struct i2c_client *client, int res)
{
	switch (res) {
	case CAM_RES_VGA:	/* fall through */
		s3c_fimc_i2c_write_addr16(client, 0xFE46, 0x05);//Preview/Movie image size($05: VGA(640x480));
		s3c_fimc_i2c_write_addr16(client, 0xFE45, 0x01);//Camera_ImageOutputMode($01: Preview);
	case CAM_RES_DEFAULT:	/* fall through */
		s3c_fimc_i2c_write_addr16(client, 0xFE46, 0x05);//Preview/Movie image size($05: VGA(640x480));
		s3c_fimc_i2c_write_addr16(client, 0xFE45, 0x01);//Camera_ImageOutputMode($01: Preview);
	case CAM_RES_MAX:	/* fall through */
		//s3c_fimc_i2c_write_addr16(client, 0xFE48, 0x00);//Number of still image output($00: Continuous (default));
		//s3c_fimc_i2c_write_addr16(client, 0xFE47, 0x0B);//Still image size($0B: QXGA( 2048 x 1536 )(default));
		//s3c_fimc_i2c_write_addr16(client, 0xFE45, 0x03);//Camera_ImageOutputMode($03: Still YUV);
		break;

	default:
		err("unexpect value\n");
	}

	return 0;
}

static int s5k3ba_change_whitebalance(struct i2c_client *client, enum s3c_fimc_wb_t type)
{
//	s3c_fimc_i2c_write(client, 0xfc, 0x0);
//	s3c_fimc_i2c_write(client, 0x30, type);

	return 0;
}

/*add by liuyihui 2009-08-24*/
/*
*设置寄存器
*/

static int s5k3ba_set_output_format(struct i2c_client *client, int res)
{
	int i;

m8_camera_dbg("res==%d\n\n",res);//add by liuyihui test

	switch (res) {
	case CAM_CAPTURE_DEFAULT:
		break;
	case CAM_CAPTURE_QSVGA:
		break;
	case CAM_CAPTURE_VGA:
		break;
	case CAM_CAPTURE_SVGA:
		break;
	case CAM_CAPTURE_SXGA:
		break;
	case CAM_CAPTURE_UXGA:
		break;
	case CAM_CAPTURE_QXGA:// Quadruple extended graphics adapter (2048 x 1536 pixels, 3.1 megapixels)

	for (i = 0; i < S5K3BA_OUTPUT_FORMAT_REGS16; i++) {
		s3c_fimc_i2c_write_addr16(client, s5k3ba_output_format_QXGA_reg16[i].subaddr, s5k3ba_output_format_QXGA_reg16[i].value);
	}
		break;
	case CAM_CAPTURE_MAX:

		break;
	default:
		err("unexpect capture value\n");
	}

	return 0;
}

static int s5k3ba_capture(struct i2c_client *client, int res)
{
	int i;
	u8 tmpbuf;
	
m8_camera_dbg("res==%d\n\n",res);//add by liuyihui test

	switch (res) {
	case CAM_CAPTURE_DEFAULT:
		break;
	case CAM_CAPTURE_QSVGA:
		break;
	case CAM_CAPTURE_VGA:
		break;
	case CAM_CAPTURE_SVGA:
		break;
	case CAM_CAPTURE_SXGA:
		break;
	case CAM_CAPTURE_UXGA:
		break;
	case CAM_CAPTURE_QXGA:// Quadruple extended graphics adapter (2048 x 1536 pixels, 3.1 megapixels)
	//	s3c_fimc_i2c_write_addr16(client, 0xFE45, 0x03);
	//自动对焦，并等待对焦状态为 Lock
	i=0;
	while(1){
			tmpbuf = s3c_fimc_i2c_read_addr16(client, 0xFE32);//read AF status
			if(0x01 == tmpbuf){//$01: AF Locked
				break;
				}
			mdelay(50);
			i++;
			if(100 <= i){//等待5秒超时
				break;
				}
		}

	for (i = 0; i < S5K3BA_START_CAPTURE_REGS16; i++) {
		s3c_fimc_i2c_write_addr16(client, s5k3ba_start_capture_reg16[i].subaddr, s5k3ba_start_capture_reg16[i].value);
	}
		break;
	case CAM_CAPTURE_MAX:

		break;
	default:
		err("unexpect capture value\n");
	}

	return 0;
}
/*add end*/

static int s5k3ba_command(struct i2c_client *client, u32 cmd, void *arg)
{
	m8_camera_dbg("cmd==%d\n\n",cmd);//add by liuyihui test
	switch (cmd) {
	case I2C_CAM_INIT:
		s5k3ba_start(client);
		info("external camera initialized\n");
		break;

	case I2C_CAM_RESOLUTION:
		s5k3ba_change_resolution(client, (int) arg);
		break;

	case I2C_CAM_WB:
		s5k3ba_change_whitebalance(client, (enum s3c_fimc_wb_t) arg);
        	break;
			
	case I2C_CAM_FOCUS_AUTO:

        	break;
	case I2C_CAM_OUTPUT_FORMAT://add by liuyihui 09-10-13
		s5k3ba_set_output_format(client, (int) arg);
		mdelay(500);//add by hui test
        	break;

	case I2C_CAM_CAPTURE://add by liuyihui 09-10-13
		s5k3ba_capture(client, (int) arg);
        	break;
			
	default:
		err("unexpect command\n");
		break;
	}

	return 0;
}

static struct i2c_driver s5k3ba_i2c_driver = {
	.driver = {
		.name = "s5k3ba",
	},
	.id = I2C_DRIVERID_S5K3BA,
	.attach_adapter = s5k3ba_attach_adapter,
	.detach_client = s5k3ba_detach,
	.command = s5k3ba_command,
};



static __init int s5k3ba_init(void)
{
	return i2c_add_driver(&s5k3ba_i2c_driver);
}

static __init void s5k3ba_exit(void)
{
	m8_camera_dbg("...\n");//add by hui for test

	s3c_fimc_unregister_camera(&s5k3ba_data);

	mdelay(200);	//add by hui 09-09-22,延时让摄像头重启初始化成功

	i2c_del_driver(&s5k3ba_i2c_driver);

	kfree(s5k3ba_data.client);

	m8_camera_dbg("...\n");//add by hui for test

}

module_init(s5k3ba_init)
module_exit(s5k3ba_exit)

MODULE_AUTHOR("Jinsung, Yang <jsgood.yang@samsung.com>");
MODULE_DESCRIPTION("Samsung S5K3BA I2C based CMOS Image Sensor driver");
MODULE_LICENSE("GPL");

