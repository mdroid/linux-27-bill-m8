/* linux/drivers/media/video/samsung/s5k3ba.h
 *
 * Header file for Samsung S5K3BA CMOS Image Sensor driver
 *
 * Jinsung Yang, Copyright (c) 2009 Samsung Electronics
 * 	http://www.samsungsemi.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#ifndef _S5K3BA_H_
#define _S5K3BA_H_

typedef struct s3c_fimc_i2c_value {
	u16 subaddr;
	u8 value;
} s5k3ba_t;

/*add by liuyihui 2009-10-13*/
/*
*摄像头拍照分辨率结构体
*/
/* init */
s5k3ba_t s5k3ba_init_reg16[] =
{
 //1. initial setting 
    {0xfe49, 03},//YCBYCR_UNSIGNED
//    {0xfe46, 0x05},//PREVIEW_SIZE_VGA
    {0xfe45, 0x01}, //Camera_ImageOutputMode(PREVIEW_MODE);
};

/* set output format QXGA (2048 x 1536 pixels, 3.1 megapixels) */
s5k3ba_t s5k3ba_output_format_QXGA_reg16[] =
{
//    {0xfe49, 0x03},//Output format:$03: YCrYCb (Unsigned)
//    {0xfe44, 0x01}, //Mirror on
    {0xfe47, 0x0B},//Still image size $0B: QXGA( 2048 x 1536 )(default)
//	{0xfe70, 0x01},//DIS function. $01: Enable DIS
    {0xFE31, 0x01},//AF Start ,$01: Start
//    {0xfe45, 0x03}, //Camera_ImageOutputMode($03: Still YUV);
};
/*add end*/	

/* capture Quadruple extended graphics adapter (2048 x 1536 pixels, 3.1 megapixels) */
/*Basic sequence for take image
1) Boot ISP FW
2) Set preview 0xFE45 0x01 write
3) AF start: 0xFE31 0x01 write
4) Check AF status: 0xFE32 read (lock or failed)
5) Stop AE: 0xFE10 0x01 write
6) Stop AWB 0xFE20 0x01 write
7) Change to still: 0xFE45 0x03 write (Preview to still)
8) Capture image: HOST
9) Change to preview: 0xFE45 0x01 write (Still to Preview)
10) Start AE: 0xFE10 0x00 write
11) Start AWB: 0xFE20 0x00 write
12) Repeat 3)～11)
*/
s5k3ba_t s5k3ba_start_capture_reg16[] =
{
	{0xFE48, 0x00},//Number of still image output($00: Continuous (default));
	{0xFE10, 0x01},//5) Stop AE: 0xFE10 0x01 write
	{0xFE20, 0x01},//6) Stop AWB 0xFE20 0x01 write
	{0xFE45, 0x03},//Camera_ImageOutputMode($03: Still YUV);
};
/*add end*/	


#define S5K3BA_INIT_REGS16	(sizeof(s5k3ba_init_reg16) / sizeof(s5k3ba_init_reg16[0]))
#define S5K3BA_OUTPUT_FORMAT_REGS16	(sizeof(s5k3ba_output_format_QXGA_reg16) / sizeof(s5k3ba_output_format_QXGA_reg16[0]))
#define S5K3BA_START_CAPTURE_REGS16	(sizeof(s5k3ba_start_capture_reg16) / sizeof(s5k3ba_start_capture_reg16[0]))

#endif

