/* linux/drivers/hwmon/m8accsensor.h
 * 
*/

#ifndef _lis302dltr_H_
#define _lis302dltr_H_

/* 1-byte registers */
#define LIS_SINGLE_ID	0x3B /* LIS[32]02DL and others */


struct lis302dltr_data {
	struct i2c_client client;
	struct semaphore update_lock;
	unsigned long last_updated; /* last update in jiffies */
	unsigned int x_val;
	unsigned int y_val;
	unsigned int inter_reg;
	unsigned long interval_time;
};

enum lis3_reg {
	WHO_AM_I	= 0x0F,
	OFFSET_X	= 0x16,
	OFFSET_Y	= 0x17,
	OFFSET_Z	= 0x18,
	GAIN_X		= 0x19,
	GAIN_Y		= 0x1A,
	GAIN_Z		= 0x1B,
	CTRL_REG1	= 0x20,
	CTRL_REG2	= 0x21,
	CTRL_REG3	= 0x22,
	
	FF_WU_CFG_1 = 0x30,
	FF_WU_THS_1 = 0x32,
	FF_WU_DURATION_1 =0x33,
	FF_WU_SRC_1 = 0x31,

	FF_WU_CFG_2 = 0x34,
	FF_WU_THS_2 = 0x36,
	FF_WU_DURATION_2 =0x37,
	FF_WU_SRC_2 = 0x35,	

	OUTX		= 0x29,
	OUTY		= 0x2B,
	OUTZ		= 0x2D,

};



#endif

