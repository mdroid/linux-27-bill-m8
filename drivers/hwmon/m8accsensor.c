/* linux/drivers/hwmon/m8accsensor.c
 * 
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/i2c-id.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/init.h>
#include <asm/io.h>


#include <mach/gpio.h>
#include <plat/gpio-cfg.h>
#include <plat/regs-gpio.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include "m8accsensor.h"  
#include <linux/ctype.h>
#include <linux/delay.h>
#include <mach/gpio.h>
#include <plat/gpio-cfg.h>
#include <plat/regs-gpio.h>

#define M8_ACC_DEBUG
#ifdef  M8_ACC_DEBUG
#define	m8_acc_dbg(fmt, arg...)    printk(KERN_INFO "m8_acc_sensor: %s(line %d): " fmt "\n", 	\
	__FUNCTION__, __LINE__, ##arg)
#else
#define	m8_acc_dbg(fmt, arg...)	do {} while (0)
#endif

#define M8_ACCSENSOR_NAME	"m8accsensor"
#define info(args...)	do { printk(KERN_INFO M8_ACCSENSOR_NAME ": " args); } while (0)
#define err(args...)	do { printk(KERN_ERR  M8_ACCSENSOR_NAME ": " args); } while (0)

#define lis302dltr_I2C_ADDR		0x1d	

const static u16 ignore[] = { I2C_CLIENT_END };
const static u16 normal_addr[] = {lis302dltr_I2C_ADDR, I2C_CLIENT_END };
const static u16 *forces[] = { NULL };
static struct i2c_driver lis302dltr_i2c_driver;


static struct i2c_client_address_data addr_data = {
	.normal_i2c	= normal_addr,
	.probe		= ignore,
	.ignore		= ignore,
	.forces		= forces,
};

//// i2c operation////
int lis302dltr_i2c_read(struct i2c_client *client, u8 subaddr)
{
	u8 buf[1];
	struct i2c_msg msg = {client->addr, 0, 1, buf};
	int ret;
	
	buf[0] = subaddr;

	ret = i2c_transfer(client->adapter, &msg, 1) == 1 ? 0 : -EIO;
	if (ret == -EIO)
	{
		err("i2c read error\n");
		return -EIO;
	}

	msg.flags = I2C_M_RD;
	ret = i2c_transfer(client->adapter, &msg, 1) == 1 ? 0 : -EIO;
	if (ret == -EIO) 
	{
		err("i2c read error\n");
		return -EIO;
	}
	
	return buf[0];
}

int lis302dltr_i2c_write(struct i2c_client *client, u8 subaddr, u8 val)
{
	u8 buf[2];
	int ret;
	struct i2c_msg msg = {client->addr, 0, 2, buf};

	buf[0] = subaddr;
	buf[1] = val;

	ret =i2c_transfer(client->adapter, &msg, 1) == 1 ? 0 : -EIO;
	if (ret == -EIO) {
		err("i2c write error\n");
		
	}
	
	return ret;
}

//// atribute xy_val ////
static struct lis302dltr_data *lis302dltr_update_device(struct device *dev)
{	
	int iret;
 	struct i2c_client *client = to_i2c_client(dev);
	struct lis302dltr_data *data = i2c_get_clientdata(client);

	down(&data->update_lock);

	if (time_after(jiffies, data->last_updated + HZ)) {

		iret = lis302dltr_i2c_read(client, OUTX); 
		if (iret == -EIO) 
		{
			err("read OUTX error\n");
			return NULL;
		}
			
		data->x_val= iret; 		
		m8_acc_dbg("read OUTX :%d\n ", data->x_val);
		

		iret = lis302dltr_i2c_read(client, OUTY); 
		if (iret == -EIO) 
		{
			err("read OUTY error\n");
			return NULL;
		}
			
		data->y_val= iret;  		
		m8_acc_dbg("read OUTX :%d\n ", data->y_val);
		
		
		data->last_updated = jiffies;
	}

	up(&data->update_lock);

	return data;
}


static ssize_t show_xy(struct device *dev, struct device_attribute *attr,char *buf)
{
	struct lis302dltr_data *data = lis302dltr_update_device(dev);
	if(NULL != data)
	{
		return sprintf(buf, "x_val = %ld; y_val = %ld\n", data->x_val,data->y_val);

	}
	else
	{
		m8_acc_dbg("failed to get the value of x and y!\n ");
		return 0;
	}
	
	
}

static DEVICE_ATTR(xy_val, S_IRUGO, show_xy, NULL);

//// atribute inter_reg ////
static ssize_t lis302dltr_reg_show(struct device *dev, 
				   struct device_attribute *attr, char *buf)
{
	int iret;
	struct i2c_client *client = to_i2c_client(dev);
	struct lis302dltr_data *data = i2c_get_clientdata(client);
	
	
	iret= lis302dltr_i2c_read(client, CTRL_REG1);  
	if (iret == -EIO) 
	{
		err("read CTRL_REG1 error\n");
		return 0;
	}

	
	data->inter_reg = iret;

	return snprintf(buf, PAGE_SIZE, "Inter_reg = 0x%02x\n", data->inter_reg);
}

static ssize_t lis302dltr_reg_store(struct device *dev, 
				    struct device_attribute *attr,
				    const char *buf, size_t count)
{
	int iret; 
	unsigned int val;
	char *endp;
	struct i2c_client *client = to_i2c_client(dev);
	struct lis302dltr_data *data = i2c_get_clientdata(client);
	
	val = simple_strtoul(buf, &endp, 0);
	if (*endp && !isspace(*endp))
	{
		return -EINVAL;
	}
		
	iret = lis302dltr_i2c_write(client,CTRL_REG1, val);
	if (iret != -EIO) 
	{		
		data->inter_reg = val;
	}
	
	m8_acc_dbg("lis302dltr CTRL_REG1 : 0x%02x\n", data->inter_reg);
	return count;
}

static DEVICE_ATTR(inter_reg, 0666,lis302dltr_reg_show,lis302dltr_reg_store);


////
static void init_acc_gpio()
{
		
	// config sda and scl 	
    s3c_gpio_cfgpin(S3C64XX_GPB(5), S3C_GPIO_SFN(2)); 
	s3c_gpio_setpull(S3C64XX_GPB(5), S3C_GPIO_PULL_DOWN); 

	s3c_gpio_cfgpin(S3C64XX_GPB(6), S3C_GPIO_SFN(2)); 
	s3c_gpio_setpull(S3C64XX_GPB(6), S3C_GPIO_PULL_DOWN); 

}

static int lis302dltr_detect(struct i2c_adapter *adap, int addr, int kind)
{

	struct i2c_client *new_client;
	struct lis302dltr_data *data;
	int err = 0;
	int value;
	int iret;


	if (!i2c_check_functionality(adap, I2C_FUNC_I2C))
		goto exit;

	if (!(data = kmalloc(sizeof(struct lis302dltr_data), GFP_KERNEL))) 
	{
		iret = -ENOMEM;
		goto exit;
	}
	memset(data, 0, sizeof(struct lis302dltr_data));

	new_client = &data->client;
	i2c_set_clientdata(new_client, data);
	new_client->addr = addr;
	new_client->adapter = adap;
	new_client->driver = &lis302dltr_i2c_driver;
	new_client->flags = 0;
	strlcpy(new_client->name, "lis302dltr", I2C_NAME_SIZE);

	init_MUTEX(&data->update_lock);

	
	data->last_updated = jiffies;

	if ((iret = i2c_attach_client(new_client)))
	{
		err("i2c_attach_client failure!\n");
		goto exit_free;
	}

	init_acc_gpio();
	
	iret = lis302dltr_i2c_write(new_client, CTRL_REG1, 0x47);  //47: enable x y z ;  43 : enable x y 
	if (iret != -EIO) 
	{		
		data->inter_reg = 0x47;
	}	
	m8_acc_dbg("lis302dltr CTRL_REG1 <= 0x%02x\n", data->inter_reg);
	
	device_create_file(&new_client->dev, &dev_attr_xy_val);
	device_create_file(&new_client->dev,&dev_attr_inter_reg);
	
	return 0;

exit_free:
	kfree(data);
exit:
	return iret;

 	
}

static int lis302dltr_attach_adapter(struct i2c_adapter *adap)
{
	int iret = 0;
	
	iret = i2c_probe(adap, &addr_data, lis302dltr_detect);
	if (iret) {
		err("failed to attach lis302dltr driver\n");
		iret = -ENODEV;
	}

	return iret;
}

static int lis302dltr_detach(struct i2c_client *client)
{
	lis302dltr_i2c_write(client, CTRL_REG1, 0x00);  //00: power down mode 
	
	device_remove_file(&client->dev, &dev_attr_xy_val);
	device_remove_file(&client->dev,&dev_attr_inter_reg);
	
	i2c_detach_client(client);
	kfree(i2c_get_clientdata(client));
	
	return 0;
}

#ifdef CONFIG_PM
static int lis302dltr_suspend(struct device *dev)
{
	int iret;
	struct i2c_client *client = to_i2c_client(dev);
	struct lis302dltr_data *data = i2c_get_clientdata(client);
	
	iret = lis302dltr_i2c_write(client, CTRL_REG1, 0x00);  //00: power down mode 
	if (iret != -EIO) 
	{		
		data->inter_reg = 0x00;
		iret =0;
	}	
	
	return iret;
}

static int lis302dltr_resume(struct device *dev)
{
	int iret;
	struct i2c_client *client = to_i2c_client(dev);
	struct lis302dltr_data *data = i2c_get_clientdata(client);
	
	iret = lis302dltr_i2c_write(client, CTRL_REG1, 0x47);  //47: enable x y z ;  43 : enable x y  
	if (iret != -EIO) 
	{		
		data->inter_reg = 0x00;
		iret =0;
	}	
	
	return iret;
}
#else
#define lis302dltr_suspend NULL
#define lis302dltr_resume NULL
#endif

static struct i2c_driver lis302dltr_i2c_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name  = "lis302dltr",
		.suspend	= lis302dltr_suspend,
		.resume 	= lis302dltr_resume,
	},
//	.id = I2C_DRIVERID_LIS302,
	.attach_adapter = lis302dltr_attach_adapter,
	.detach_client  = lis302dltr_detach,
	
};


static __init int lis302dltr_init(void)
{	
	return i2c_add_driver(&lis302dltr_i2c_driver);
}

static __init void lis302dltr_exit(void)
{
	i2c_del_driver(&lis302dltr_i2c_driver);
}

MODULE_AUTHOR("jack <lih@meizu.com>");
MODULE_DESCRIPTION("I2C based accelerometer sensor driver for STMicroelectronics's lis302dltr");
MODULE_LICENSE("GPL");

module_init(lis302dltr_init)
module_exit(lis302dltr_exit)


