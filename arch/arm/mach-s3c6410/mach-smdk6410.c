/* linux/arch/arm/mach-s3c6410/zmach-smdk6410.c
 *
 * Copyright 2008 Openmoko, Inc.
 * Copyright 2008 Simtec Electronics
 *	Ben Dooks <ben@simtec.co.uk>
 *	http://armlinux.simtec.co.uk/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
*/

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/list.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/serial_core.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/i2c.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/android_pmem.h>	//sy82.yoon - pmem reserved.(2009.04.21)
#include <linux/spi/spi.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>

#include <mach/hardware.h>
#include <mach/map.h>
#include <mach/regs-mem.h>

#include <asm/setup.h>
#include <asm/irq.h>
#include <asm/mach-types.h>

#include <plat/regs-serial.h>
#include <plat/iic.h>

// andyEdit - 2.6.28 Camera driver
#include <plat/fimc.h>

// 2009.04.30 added by hyunkyung
#include <plat/regs-rtc.h>

#include <plat/nand.h>
#include <plat/partition.h>
#include <plat/s3c6410.h>
#include <plat/clock.h>
#include <plat/regs-clock.h>
#include <plat/devs.h>
#include <plat/cpu.h>
#include <plat/ts.h>
#include <plat/adc.h>
#include <plat/reserved_mem.h>
#include <plat/pm.h>

#include <mach/gpio.h>/*add by liuyihui 2009-08-24*/
#include <plat/gpio-cfg.h>/*add by liuyihui 2009-08-24*/
#include <plat/regs-gpio.h>/*add by liuyihui 2009-08-24*/



#define UCON S3C2410_UCON_DEFAULT | S3C2410_UCON_UCLK
#define ULCON S3C2410_LCON_CS8 | S3C2410_LCON_PNONE | S3C2410_LCON_STOPB
#define UFCON S3C2410_UFCON_RXTRIG8 | S3C2410_UFCON_FIFOMODE

extern struct sys_timer s3c_timer;
extern void s3c64xx_reserve_bootmem(void);

//static struct samspi_device spidev_b0_cs0;
#if 0
static struct samspi_device ProtocolADriver_b1_cs0;
static struct samspi_device spidev_b0_cs1;
static struct samspi_device ProtocolBDriver_b1_cs1;
static struct samspi_device spidev_b1_cs2;
#endif

static struct spi_board_info __initdata sam_spi_devs[] = {
	[0] = {
		.modalias	 = "spidev", /* Test Interface */
		.mode		 = SPI_MODE_2,	/* CPOL=1, CPHA=0 */
		.max_speed_hz	 = 2468013,
		/* Connected to SPI-0 as 1st Slave */
		.bus_num	 = 0,
		.irq		 = IRQ_SPI0,
		.chip_select	 = 0,
//		.controller_data = (void *)&spidev_b0_cs0,
#if 0
	}, {
		.modalias	= "ProtocolADriver",
		.mode		= SPI_MODE_2,
		.max_speed_hz	= 1357923,
		/* Connected to SPI-1 as 1st Slave */
		.bus_num	= 1,
		.irq		= IRQ_SPI1,
		.chip_select	= 0,
		.controller_data = (void *)&ProtocolADriver_b1_cs0,
	}, {
		.modalias	= "spidev",
		.mode		= SPI_MODE_2,
		.max_speed_hz	= 2357923,
		/* Connected to SPI-0 as 2nd Slave */
		.bus_num	= 0,
		.irq		= IRQ_SPI0,
		.chip_select	= 1,
		.controller_data = (void *)&spidev_b0_cs1,
	}, {
		.modalias	= "ProtocolBDriver",
		.mode		= SPI_MODE_2,
		.max_speed_hz	= 3357923,
		/* Connected to SPI-1 as 2ndst Slave */
		.bus_num	= 1,
		.irq		= IRQ_SPI1,
		.chip_select	= 1,
		.controller_data = (void *)&ProtocolBDriver_b1_cs1,
	}, {
		.modalias	= "spidev",
		.mode		= SPI_MODE_2,
		.max_speed_hz	= 4357923,
		/* Connected to SPI-1 as 3rd Slave */
		.bus_num	= 1,
		.irq		= IRQ_SPI1,
		.chip_select	= 2,
		.controller_data = (void *)&spidev_b1_cs2,
#endif
	},
};

static struct s3c2410_uartcfg smdk6410_uartcfgs[] __initdata = {
	[0] = {
		.hwport	     = 0,
		.flags	     = 0,
		.ucon	     = S3C64XX_UCON_DEFAULT,
		.ulcon	     = S3C64XX_ULCON_DEFAULT,
		.ufcon	     = S3C64XX_UFCON_DEFAULT,
	},
	[1] = {
		.hwport	     = 1,
		.flags	     = 0,
		.ucon	     = S3C64XX_UCON_DEFAULT,
		.ulcon	     = S3C64XX_ULCON_DEFAULT,
		.ufcon	     = S3C64XX_UFCON_DEFAULT,
	},
	[2] = {
		.hwport	     = 2,
		.flags	     = 0,
		.ucon	     = S3C64XX_UCON_DEFAULT,
		.ulcon	     = S3C64XX_ULCON_DEFAULT,
		.ufcon	     = S3C64XX_UFCON_DEFAULT,
	},
	[3] = {
		.hwport	     = 3,
		.flags	     = 0,
		.ucon	     = S3C64XX_UCON_DEFAULT,
		.ulcon	     = S3C64XX_ULCON_DEFAULT,
		.ufcon	     = S3C64XX_UFCON_DEFAULT,
	},
};

struct map_desc smdk6410_iodesc[] = {};

  struct platform_device sec_device_backlight = {
	.name	= "smdk-backlight",
	.id		= -1,
};

#ifdef CONFIG_ANDROID_PMEM
// sy82.yoon - pmem initialize (2009.04.21)
static struct android_pmem_platform_data pmem_pdata = {
	.name = "pmem",
	.no_allocator = 1,
	.cached = 1,
	.start = PMEM_RESERVED_MEM_START,
//	.size = 0x800000,
	.size = 0x1000000	// sy82.yoon - test for 3D game(expand 16MB for pmem)
};

static struct android_pmem_platform_data pmem_gpu1_pdata = {
	.name = "pmem_gpu1",
	.no_allocator = 1,
	.cached = 1,
	.buffered = 1,
	.start = GPU_RESERVED_MEM_START,
	.size = 0x200000,
};

static struct platform_device pmem_device = {
	.name = "android_pmem",
	.id = 0,
	.dev = { .platform_data = &pmem_pdata },
};
static struct platform_device pmem_gpu1_device = {
	.name = "android_pmem",
	.id = 1,
	.dev = { .platform_data = &pmem_gpu1_pdata },
};
#endif


static struct platform_device *smdk6410_devices[] __initdata = {
#ifdef CONFIG_SMDK6410_SD_CH0
	&s3c_device_hsmmc0,
#endif
#ifdef CONFIG_SMDK6410_SD_CH1
	&s3c_device_hsmmc1,
#endif
	&s3c_device_i2c0,
	&s3c_device_i2c1,
	&s3c_device_spi0,
	&s3c_device_spi1,
//	&s3c_device_ts,	/*commented by liuyihui 2009-08-27*/
//	&synaptics510_device_ts,
	//&s3c_device_smc911x,
	&s3c_device_lcd,
	//&s3c_device_nand,
	&s3c_device_onenand,	/*add by liuyihui 2009-08-21*/
//	&s3c_device_keypad,
	&meizu_m8_buttons,		/*modified by cefanty*/
	&s3c_device_usbgadget,
#ifdef CONFIG_S3C64XX_ADC
	&s3c_device_adc,
#endif
#ifdef CONFIG_RTC_DRV_S3C6410
	&s3c_device_rtc,
#endif
	&s3c_device_2d,
	&s3c_device_accelerometer, //add by lih 09-10-08 accelerometer sensor 
	// andyEdit - 2.6.28 Camera driver
	&s3c_device_fimc0,
	&s3c_device_fimc1,

	&s3c_device_mfc,
	&s3c_device_g3d,
	&s3c_device_rotator,
	&s3c_device_jpeg,
	&s3c_device_vpp,
// 2009.03.24 add device for tvout by hyunkyung
#ifdef CONFIG_S3C6410_TVOUT
	&s3c_device_tvenc,
	&s3c_device_tvscaler,
#endif
	&sec_device_backlight,
	// sy82.yoon - using pmem & pmem_gpu1 (2009.04.21)
#ifdef CONFIG_ANDROID_PMEM
	&pmem_device,
	&pmem_gpu1_device
#endif

};

static struct i2c_board_info i2c_devs0[] __initdata = {
	{ I2C_BOARD_INFO("24c08", 0x50), },
	//{ I2C_BOARD_INFO("WM8753", 0x1a), }	//add by cefanty 2009-09-05 增加WM8753 Audio 支持
};

static struct i2c_board_info i2c_devs1[] __initdata = {
	//{ I2C_BOARD_INFO("24c128", 0x57), },	/* Samsung S524AD0XD1 */
	{ I2C_BOARD_INFO("synaptics-rmi-ts", 0x20), },	/*meizu m8 touch pannel controller*/
};

static struct s3c_ts_mach_info s3c_ts_platform __initdata = {
	.delay 			= 10000,
	.presc 			= 49,
	.oversampling_shift	= 2,
	.resol_bit 		= 12,
	.s3c_adc_con		= ADC_TYPE_2,
};

static struct s3c_adc_mach_info s3c_adc_platform = {
	/* s3c6410 support 12-bit resolution */
	.delay	= 	10000,
	.presc 	= 	49,
	.resolution = 	12,
};

/*
*需要把GSM  的LDO 拉高给其供电
*GPD4 拉高
*/
int smdk6410_baseband_power(int poweron)
{
	unsigned int gpio;
	gpio = S3C64XX_GPD(4);
	s3c_gpio_setpull(gpio, S3C_GPIO_PULL_DOWN);
	s3c_gpio_setpin(gpio, poweron&0x01);
	s3c_gpio_cfgpin(gpio, S3C_GPIO_OUTPUT);
	s3c_gpio_setpin(gpio, poweron&0x01);
	return 0;
}
EXPORT_SYMBOL(smdk6410_baseband_power);

/*
*拉高GPK14,给usb上电
*/
static void __init smdk6410_usb_power(int value){

	unsigned int gpio = S3C64XX_GPK(14);
	s3c_gpio_setpull(gpio, S3C_GPIO_PULL_DOWN);
	s3c_gpio_setpin(gpio, value&0x01);
	s3c_gpio_cfgpin(gpio, S3C_GPIO_OUTPUT);
	s3c_gpio_setpin(gpio, value&0x01);
}


static void __init smdk6410_map_io(void)
{
	s3c_device_nand.name = "s3c6410-nand";

	s3c64xx_init_io(smdk6410_iodesc, ARRAY_SIZE(smdk6410_iodesc));
	s3c24xx_init_clocks(12000000);
	s3c24xx_init_uarts(smdk6410_uartcfgs, ARRAY_SIZE(smdk6410_uartcfgs));
}

static void __init smdk6410_smc911x_set(void)
{
	unsigned int tmp;

	tmp = __raw_readl(S3C64XX_SROM_BW);
	tmp &= ~(S3C64XX_SROM_BW_WAIT_ENABLE1_MASK | S3C64XX_SROM_BW_WAIT_ENABLE1_MASK |
		S3C64XX_SROM_BW_DATA_WIDTH1_MASK);
	tmp |= S3C64XX_SROM_BW_BYTE_ENABLE1_ENABLE | S3C64XX_SROM_BW_WAIT_ENABLE1_ENABLE |
		S3C64XX_SROM_BW_DATA_WIDTH1_16BIT;

	__raw_writel(tmp, S3C64XX_SROM_BW);

	__raw_writel(S3C64XX_SROM_BCn_TACS(0) | S3C64XX_SROM_BCn_TCOS(4) |
			S3C64XX_SROM_BCn_TACC(13) | S3C64XX_SROM_BCn_TCOH(1) |
			S3C64XX_SROM_BCn_TCAH(4) | S3C64XX_SROM_BCn_TACP(6) |
			S3C64XX_SROM_BCn_PMC_NORMAL, S3C64XX_SROM_BC1);
}

static void __init smdk6410_fixup (struct machine_desc *desc, struct tag *tags,
	      char **cmdline, struct meminfo *mi)
{
	/*
	 * Bank start addresses are not present in the information
	 * passed in from the boot loader.  We could potentially
	 * detect them, but instead we hard-code them.
	 */
	mi->bank[0].start = PHYS_OFFSET;
	mi->bank[1].start = PHYS_OFFSET1;

#if defined(CONFIG_RESERVED_MEM_JPEG)
	mi->bank[0].size = 120*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_JPEG_POST)
	mi->bank[0].size = 112*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_MFC)
	mi->bank[0].size = 122*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_MFC_POST)
	mi->bank[0].size = 114*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_JPEG_MFC_POST)
	mi->bank[0].size = 106*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_JPEG_CAMERA)
	mi->bank[0].size = 105*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_JPEG_POST_CAMERA)
	mi->bank[0].size = 97*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_MFC_CAMERA)
	mi->bank[0].size = 107*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_MFC_POST_CAMERA)
	mi->bank[0].size = 99*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_JPEG_MFC_POST_CAMERA)
	mi->bank[0].size = 91*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_CMM_MFC_POST)
	mi->bank[0].size = 106*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_CMM_JPEG_MFC_POST_CAMERA)
	mi->bank[0].size = 83*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_TV_MFC_POST_CAMERA)
	mi->bank[0].size = 91*1024*1024;
// 2009.03.30 added for tvout by hyunkyung
#elif defined(CONFIG_RESERVED_MEM_TV_CMM_JPEG_MFC_POST_CAMERA)
         mi->bank[0].size = 87*1024*1024;
#elif defined(CONFIG_RESERVED_MEM_TV_CMM_JPEG_MFC_POST_CAMERA_GPU_PMEM)
        // mi->bank[0].size = (128 + 84)*1024*1024;	// set for 256MB RAM (added by sy82.yoon- pmem reserved memory 8MB)
        mi->bank[0].size = (128+76)*1024*1024;	// set for 256MB RAM (added by sy82.yoon - expand pmem reserved memory 16MB)
#elif defined(CONFIG_RESERVED_MEM_CMM_POST_GPU_PMEM)
	mi->bank[0].size = (128 + 114) * 1024 * 1024;
#else
 	mi->bank[0].size = 128*1024*1024;
#endif
	mi->bank[0].node = 0;
	mi->nr_banks = 1;	//orig:1	/*modified by liuyihui 2009-08-25*/
}

static void __init smdk6410_machine_init(void)
{
	s3c_device_nand.dev.platform_data = &s3c_nand_mtd_part_info;
	s3c_device_onenand.dev.platform_data = &s3c_nand_mtd_part_info;

	smdk6410_smc911x_set();

	s3c_i2c0_set_platdata(NULL);
	s3c_i2c1_set_platdata(NULL);

//	s3c_ts_set_platdata(&s3c_ts_platform);	/*commented by liuyihui 2009-08-27*/
//	synaptics510_ts_set_platdata(&synaptics510_ts_platform);	/*commented by liuyihui 2009-08-27*/
	s3c_adc_set_platdata(&s3c_adc_platform);

	i2c_register_board_info(0, i2c_devs0, ARRAY_SIZE(i2c_devs0));
	i2c_register_board_info(1, i2c_devs1, ARRAY_SIZE(i2c_devs1));

	spi_register_board_info(sam_spi_devs, ARRAY_SIZE(sam_spi_devs));

// andyEdit - 3.6.28 Camera driver
	s3c_fimc0_set_platdata(NULL);
	s3c_fimc1_set_platdata(NULL);
#ifdef CONFIG_VIDEO_FIMC
//	s3c_fimc_reset_camera();	//marked by liuyihui 09-10-12
#endif

	platform_add_devices(smdk6410_devices, ARRAY_SIZE(smdk6410_devices));
	s3c6410_pm_init();
	
	/*
	*拉高GPK14,给usb上电
	*拉高GPD4,给baseband 上电
	*/
	smdk6410_usb_power(1);
	smdk6410_baseband_power(1);
}

MACHINE_START(SMDK6410, "SMDK6410")
	/* Maintainer: Ben Dooks <ben@fluff.org> */
	.phys_io	= S3C_PA_UART & 0xfff00000,
	.io_pg_offst	= (((u32)S3C_VA_UART) >> 18) & 0xfffc,
	.boot_params	= S3C64XX_PA_SDRAM + 0x100,
	.fixup		= smdk6410_fixup,
	.init_irq	= s3c6410_init_irq,
	.map_io		= smdk6410_map_io,
	.init_machine	= smdk6410_machine_init,
	.timer		= &s3c64xx_timer,
MACHINE_END

#if defined(CONFIG_KEYPAD_S3C) || defined (CONFIG_KEYPAD_S3C_MODULE)
void s3c_setup_keypad_cfg_gpio(int rows, int columns)
{
	unsigned int gpio;
	unsigned int end;

	end = S3C64XX_GPK(8 + rows);

	/* Set all the necessary GPK pins to special-function 0 */
	for (gpio = S3C64XX_GPK(8); gpio < end; gpio++) {
		s3c_gpio_cfgpin(gpio, S3C_GPIO_SFN(3));
		s3c_gpio_setpull(gpio, S3C_GPIO_PULL_NONE);
	}

	end = S3C64XX_GPL(0 + columns);

	/* Set all the necessary GPK pins to special-function 0 */
	for (gpio = S3C64XX_GPL(0); gpio < end; gpio++) {
		s3c_gpio_cfgpin(gpio, S3C_GPIO_SFN(3));
		s3c_gpio_setpull(gpio, S3C_GPIO_PULL_NONE);
	}
}

EXPORT_SYMBOL(s3c_setup_keypad_cfg_gpio);
#endif

// 2009.04.30 added by hyunkyung
#if defined(CONFIG_RTC_DRV_S3C6410)
/* RTC common Function for samsung APs*/
unsigned int s3c_rtc_set_bit_byte(void __iomem *base, uint offset, uint val)
{
        writeb(val, base + offset);

        return 0;
}
#endif


