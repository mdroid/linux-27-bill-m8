/*
 *  linux/arch/arm/plat-s3c64xx/s3c64xx-cpufreq.c
 *
 *  CPU frequency scaling for S3C64XX
 *
 *  Copyright (C) 2008 Samsung Electronics
 *
 *  Based on cpu-sa1110.c, Copyright (C) 2001 Russell King
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/cpufreq.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/clk.h>

#ifdef CONFIG_ANDROID_POWER
#include <linux/android_power.h>
#endif

#include <asm/system.h>
#include <plat/s3c64xx-dvfs.h>

#define CPU_FREQ_EARLY_FLAG   0x100

#if defined(CONFIG_MACH_SMDK6410)
extern int set_pmic(unsigned int pwr, unsigned int voltage);
#endif

unsigned int S3C64XX_MAXFREQLEVEL;
static unsigned int s3c64xx_cpufreq_level;
static char cpufreq_governor_name[CPUFREQ_NAME_LEN] = "userspace";
static char userspace_governor[CPUFREQ_NAME_LEN] = "userspace";
unsigned int s3c64xx_cpufreq_index = 0;

#define CLIP_LEVEL(a, b) (a > b ? b : a)

static spinlock_t g_cpufreq_lock = SPIN_LOCK_UNLOCKED;

static struct cpufreq_frequency_table freq_table_532MHz[] = {
	{0, 532*KHZ_T},
	{1, 266*KHZ_T},
	{2, 133*KHZ_T},
	{3, 133*KHZ_T},
	{4, 66*KHZ_T},
	{5, CPUFREQ_TABLE_END},
};

static struct cpufreq_frequency_table freq_table_800MHz[] = {
	{0, 800*KHZ_T},
	{1, 400*KHZ_T},
	{2, 266*KHZ_T},	
	{3, 133*KHZ_T},
	{4, 133*KHZ_T},
	{5, (66)*KHZ_T},
	{6, CPUFREQ_TABLE_END},
};

static unsigned char transition_state_800MHz[][2] = {
	{1, 0},
	{2, 0},
	{3, 1},
	{4, 2},
	{5, 3},
	{5, 4},
};

static unsigned char transition_state_532MHz[][2] = {
	{1, 0},
	{2, 0},
	{3, 1},
	{4, 2},
	{4, 3},
};

/* frequency voltage matching table */
static const unsigned int frequency_match_532MHz[][4] = {
/* frequency, Mathced VDD ARM voltage , Matched VDD INT*/
	{532000, 1100, 1200, 0},
	{266000, 1050, 1200, 1},
	{133000, 1000, 1200, 2},
	{133000, 1000, 1050, 3},
	{66000, 1000, 1050, 4},
};

/* frequency voltage matching table */
static const unsigned int frequency_match_800MHz[][4] = {
/* frequency, Mathced VDD ARM voltage , Matched VDD INT*/
	{800000, 1300, 1200, 0},
	{400000, 1100, 1200, 1},
	{266000, 1050, 1200, 2},
	{133000, 1000, 1200, 3},
	{133000, 1000, 1050, 4},
	{66000, 1000, 1050, 5},
};

static const unsigned int (*frequency_match[2])[4] = {
	frequency_match_532MHz,
	frequency_match_800MHz,
};

static unsigned char (*transition_state[2])[2] = {
	transition_state_532MHz,
	transition_state_800MHz,
};

static struct cpufreq_frequency_table *s3c6410_freq_table[] = {
	freq_table_532MHz,
	freq_table_800MHz,
};

#ifdef USE_DVS
static unsigned int s_arm_voltage, s_int_voltage;
int set_voltage(unsigned int freq_index)
{
	static int index = 0;
	unsigned int arm_voltage, int_voltage;
	
	if(index == freq_index)
		return 0;
		
	index = freq_index;
	
	arm_voltage = frequency_match[S3C64XX_FREQ_TAB][index][1];
	int_voltage = frequency_match[S3C64XX_FREQ_TAB][index][2];
	
	if(arm_voltage != s_arm_voltage) {
		set_pmic(VCC_ARM, arm_voltage);
		s_arm_voltage = arm_voltage;
	}
	if(int_voltage != s_int_voltage) {
		set_pmic(VCC_INT, int_voltage);
		s_int_voltage = int_voltage;
	}

	return 0;
}
#endif

unsigned int s3c64xx_target_frq(unsigned int pred_freq, 
				int flag)
{
	int index = s3c64xx_cpufreq_index;
	unsigned long irqflags;
	unsigned int freq;
	struct cpufreq_frequency_table *freq_tab = s3c6410_freq_table[S3C64XX_FREQ_TAB];

	if(freq_tab[0].frequency < pred_freq) {
	   index = 0;	
	   goto s3c64xx_target_frq_end;
	}

	if((flag != 1)&&(flag != -1)) {
		printk("s3c64xx_target_frq: flag error!!!!!!!!!!!!!");
	}

	if(freq_tab[index].frequency == pred_freq) {	
		if(flag == 1)
			index = transition_state[S3C64XX_FREQ_TAB][index][1];
		else
			index = transition_state[S3C64XX_FREQ_TAB][index][0];
	}
	else if(flag == -1) {
		index = 1;
	}
	else {
		index = 0; 
	}
s3c64xx_target_frq_end:
	spin_lock_irqsave(&g_cpufreq_lock, irqflags);
	index = CLIP_LEVEL(index, s3c64xx_cpufreq_level);
	s3c64xx_cpufreq_index = index;
	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
	
	freq = freq_tab[index].frequency;

	return freq;
}

int s3c64xx_target_freq_index(unsigned int freq)
{
	int index = 0;
	unsigned long irqflags;
	
	struct cpufreq_frequency_table *freq_tab = s3c6410_freq_table[S3C64XX_FREQ_TAB];

	if(freq >= freq_tab[index].frequency) {
		goto s3c64xx_target_freq_index_end;
	}

	/*Index might have been calculated before calling this function.
	check and early return if it is already calculated*/
	if(freq_tab[s3c64xx_cpufreq_index].frequency == freq) {		
		return s3c64xx_cpufreq_index;
	}

	while((freq < freq_tab[index].frequency) &&
			(freq_tab[index].frequency != CPUFREQ_TABLE_END)) {
		index++;
	}

	if(index > 0) {
		if(freq != freq_tab[index].frequency) {
			index--;
		}
	}

	if(freq_tab[index].frequency == CPUFREQ_TABLE_END) {
		index--;
	}

s3c64xx_target_freq_index_end:
	spin_lock_irqsave(&g_cpufreq_lock, irqflags);
	index = CLIP_LEVEL(index, s3c64xx_cpufreq_level);
	s3c64xx_cpufreq_index = index;
	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
	
	return index; 
} 

int is_userspace_gov(void)
{
	int ret = 0;
	unsigned long irqflags;
	spin_lock_irqsave(&g_cpufreq_lock, irqflags);

	if(!strnicmp(cpufreq_governor_name, userspace_governor, CPUFREQ_NAME_LEN)) {
		ret = 1;
	}

	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
	return ret;
}

int s3c6410_verify_speed(struct cpufreq_policy *policy)
{
#ifndef USE_FREQ_TABLE
	struct clk *mpu_clk;
#endif
	if(policy->cpu)
		return -EINVAL;
#ifdef USE_FREQ_TABLE
	return cpufreq_frequency_table_verify(policy, s3c6410_freq_table[S3C64XX_FREQ_TAB]);
#else
	cpufreq_verify_within_limits(policy, policy->cpuinfo.min_freq,
				     policy->cpuinfo.max_freq);
	mpu_clk = clk_get(NULL, MPU_CLK);

	if(IS_ERR(mpu_clk))
		return PTR_ERR(mpu_clk);

	policy->min = clk_round_rate(mpu_clk, policy->min * KHZ_T) / KHZ_T;
	policy->max = clk_round_rate(mpu_clk, policy->max * KHZ_T) / KHZ_T;

	cpufreq_verify_within_limits(policy, policy->cpuinfo.min_freq,
				     policy->cpuinfo.max_freq);

	clk_put(mpu_clk);

	return 0;
#endif
}
extern unsigned long s3c_fclk_get_rate(void);
unsigned int s3c6410_getspeed(unsigned int cpu)
{
	struct clk * mpu_clk;
	unsigned long rate;

	if(cpu)
		return 0;

	mpu_clk = clk_get(NULL, MPU_CLK);
	if (IS_ERR(mpu_clk))
		return 0;

	rate = s3c_fclk_get_rate() / KHZ_T;
	clk_put(mpu_clk);

	return rate;
}

static int s3c6410_target(struct cpufreq_policy *policy,
		       unsigned int target_freq,
		       unsigned int relation)
{
	struct clk * mpu_clk;
	struct cpufreq_freqs freqs;
	static int prevIndex = 0;
	unsigned long irqflags;
	int ret = 0;
	unsigned long arm_clk;
	unsigned int index;

	mpu_clk = clk_get(NULL, MPU_CLK);
	if(IS_ERR(mpu_clk))
		return PTR_ERR(mpu_clk);

	if(policy != NULL) {
		if(policy -> governor) {
			spin_lock_irqsave(&g_cpufreq_lock, irqflags);
			if (strnicmp(cpufreq_governor_name, policy->governor->name, CPUFREQ_NAME_LEN)) {
				strcpy(cpufreq_governor_name, policy->governor->name);
			}
			spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
		}
	}

	freqs.old = s3c6410_getspeed(0);

	if(freqs.old == s3c6410_freq_table[S3C64XX_FREQ_TAB][0].frequency) {
		prevIndex = 0;
	}
	
	index = s3c64xx_target_freq_index(target_freq);
	if(index == INDX_ERROR) {
		printk("s3c6410_target: INDX_ERROR \n");
		return -EINVAL;
	}
	
	if(prevIndex == index)
		return ret;

	arm_clk = s3c6410_freq_table[S3C64XX_FREQ_TAB][index].frequency;
	freqs.new = arm_clk;
	freqs.cpu = 0;
	freqs.new_hclk = 133000;
  
	if(index > S3C64XX_MAXFREQLEVEL) {
		freqs.new_hclk = 66000;         
	} 

	target_freq = arm_clk;

	cpufreq_notify_transition(&freqs, CPUFREQ_PRECHANGE);

	spin_lock_irqsave(&g_cpufreq_lock, irqflags);
#ifdef USE_DVS
	if(prevIndex < index) {
		/* frequency scaling */
		ret = clk_set_rate(mpu_clk, target_freq * KHZ_T);
		if(ret != 0) {
			printk("frequency scaling error\n");
			ret = -EINVAL;
			goto s3c6410_target_end;
		}
		/* voltage scaling */
		set_voltage(index);
	}
	else {
		/* voltage scaling */
		set_voltage(index);
		/* frequency scaling */
		ret = clk_set_rate(mpu_clk, target_freq * KHZ_T);
		if(ret != 0) {
			printk("frequency scaling error\n");
			ret = -EINVAL;
			goto s3c6410_target_end;
		}
	}
#else
	ret = clk_set_rate(mpu_clk, target_freq * KHZ_T);
	if(ret != 0) {
		printk("frequency scaling error\n");
		ret = -EINVAL;
		goto s3c6410_target_end;
	}
#endif
	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
	cpufreq_notify_transition(&freqs, CPUFREQ_POSTCHANGE);
	prevIndex = index;
	clk_put(mpu_clk);
	return ret;
s3c6410_target_end:
	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
	return ret;
}

int s3c6410_pm_target(unsigned int target_freq)
{
	struct clk * mpu_clk;
	int ret = 0;
	unsigned long arm_clk;
	unsigned int index;

	mpu_clk = clk_get(NULL, MPU_CLK);
	if(IS_ERR(mpu_clk))
		return PTR_ERR(mpu_clk);

	index = s3c64xx_target_freq_index(target_freq);
	if(index == INDX_ERROR) {
	   printk("s3c6410_target: INDX_ERROR \n");
	   return -EINVAL;
	}
	
	arm_clk = s3c6410_freq_table[S3C64XX_FREQ_TAB][index].frequency;
	target_freq = arm_clk;

#ifdef USE_DVS
	set_voltage(index);
#endif
	/* frequency scaling */
	ret = clk_set_rate(mpu_clk, target_freq * KHZ_T);
	if(ret != 0) {
		printk("frequency scaling error\n");
		return -EINVAL;
	}
	
	clk_put(mpu_clk);
	return ret;
}

#ifdef CONFIG_ANDROID_POWER
void s3c64xx_cpufreq_powersave(android_early_suspend_t *h)
{
	unsigned long irqflags;
	spin_lock_irqsave(&g_cpufreq_lock, irqflags);
	s3c64xx_cpufreq_level = S3C64XX_MAXFREQLEVEL + 2;
	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
	return;
}

void s3c64xx_cpufreq_performance(android_early_suspend_t *h)
{
	unsigned long irqflags;
	if(!is_userspace_gov()) {
		spin_lock_irqsave(&g_cpufreq_lock, irqflags);
		s3c64xx_cpufreq_level = S3C64XX_MAXFREQLEVEL;
		s3c64xx_cpufreq_index = CLIP_LEVEL(s3c64xx_cpufreq_index, S3C64XX_MAXFREQLEVEL);
		spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
		s3c6410_target(NULL, s3c6410_freq_table[S3C64XX_FREQ_TAB][s3c64xx_cpufreq_index].frequency, 1);
	}
	else {
		spin_lock_irqsave(&g_cpufreq_lock, irqflags);
		s3c64xx_cpufreq_level = S3C64XX_MAXFREQLEVEL;
		spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);
#ifdef USE_DVS
		set_voltage(s3c64xx_cpufreq_index);
#endif
	}

	return;
}

static struct android_early_suspend s3c64xx_freq_suspend = {
	.suspend = s3c64xx_cpufreq_powersave,
	.resume = s3c64xx_cpufreq_performance,
	.level = ANDROID_EARLY_SUSPEND_LEVEL_DISABLE_FB + 1,
};
#endif

unsigned int get_min_cpufreq(void)
{
	return (s3c6410_freq_table[S3C64XX_FREQ_TAB][S3C64XX_MAXFREQLEVEL].frequency);
}

static int __init s3c6410_cpu_init(struct cpufreq_policy *policy)
{
	struct clk * mpu_clk;
	unsigned long irqflags;

	mpu_clk = clk_get(NULL, MPU_CLK);
	if(IS_ERR(mpu_clk))
		return PTR_ERR(mpu_clk);

	if(policy->cpu != 0)
		return -EINVAL;
	policy->cur = policy->min = policy->max = s3c6410_getspeed(0);

	spin_lock_irqsave(&g_cpufreq_lock, irqflags);

	if(policy->max == MAXIMUM_FREQ) {
		S3C64XX_FREQ_TAB = 1;
		S3C64XX_MAXFREQLEVEL = 3;	
	}
	else {
		S3C64XX_FREQ_TAB = 0;
		S3C64XX_MAXFREQLEVEL = 2;	
	}
	s3c64xx_cpufreq_level = S3C64XX_MAXFREQLEVEL;

	spin_unlock_irqrestore(&g_cpufreq_lock, irqflags);

#ifdef USE_FREQ_TABLE
	cpufreq_frequency_table_get_attr(s3c6410_freq_table[S3C64XX_FREQ_TAB], policy->cpu);
#else
	policy->cpuinfo.min_freq = clk_round_rate(mpu_clk, 0) / KHZ_T;
	policy->cpuinfo.max_freq = clk_round_rate(mpu_clk, VERY_HI_RATE) / KHZ_T;
#endif
	policy->cpuinfo.transition_latency = 40000;

	clk_put(mpu_clk);

#ifdef USE_DVS
	s_arm_voltage = frequency_match[S3C64XX_FREQ_TAB][0][1];
	s_int_voltage = frequency_match[S3C64XX_FREQ_TAB][0][2];
#if defined(CONFIG_MACH_SMDK6410)
	ltc3714_init(s_arm_voltage, s_int_voltage);
#endif
#endif

#ifdef CONFIG_ANDROID_POWER
	android_register_early_suspend(&s3c64xx_freq_suspend);
#endif
#ifdef USE_FREQ_TABLE
	return cpufreq_frequency_table_cpuinfo(policy, s3c6410_freq_table[S3C64XX_FREQ_TAB]);
#else
	return 0;
#endif
}

static struct cpufreq_driver s3c6410_driver = {
	.flags		= CPUFREQ_STICKY,
	.verify		= s3c6410_verify_speed,
	.target		= s3c6410_target,
	.get			= s3c6410_getspeed,
	.init			= s3c6410_cpu_init,
	.name			= "s3c6410",
};

static int __init s3c6410_cpufreq_init(void)
{
	return cpufreq_register_driver(&s3c6410_driver);
}

device_initcall(s3c6410_cpufreq_init);
