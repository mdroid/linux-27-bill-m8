/* linux/arch/arm/plat-s3c64xx/devs.c
 *
 * Copyright 2008 Openmoko, Inc.
 * Copyright 2008 Simtec Electronics
 *	Ben Dooks <ben@simtec.co.uk>
 *	http://armlinux.simtec.co.uk/
 *
 * Base S3C64XX resource and device definitions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
*/

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/list.h>
#include <linux/platform_device.h>
#include <linux/input.h>//add by liuyihui 09-08-31
#include <linux/gpio_keys.h>//add by liuyihui 09-08-31


#include <asm/mach/arch.h>
#include <asm/mach/irq.h>
#include <mach/hardware.h>
#include <mach/map.h>

#include <plat/devs.h>
#include <plat/adc.h>


/* SMC9115 LAN via ROM interface */

static struct resource s3c_smc911x_resources[] = {
      [0] = {
              .start  = S3C64XX_PA_SMC9115,
              .end    = S3C64XX_PA_SMC9115 + S3C64XX_SZ_SMC9115 - 1,
              .flags  = IORESOURCE_MEM,
      },
      [1] = {
              .start = IRQ_EINT(10),
              .end   = IRQ_EINT(10),
              .flags = IORESOURCE_IRQ,
        },
};

struct platform_device s3c_device_smc911x = {
      .name           = "smc911x",
      .id             =  -1,
      .num_resources  = ARRAY_SIZE(s3c_smc911x_resources),
      .resource       = s3c_smc911x_resources,
};

/* NAND Controller */

static struct resource s3c_nand_resource[] = {
	[0] = {
		.start = S3C64XX_PA_NAND,
		.end   = S3C64XX_PA_NAND + S3C64XX_SZ_NAND - 1,
		.flags = IORESOURCE_MEM,
	}
};

struct platform_device s3c_device_nand = {
	.name		  = "s3c-nand",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_nand_resource),
	.resource	  = s3c_nand_resource,
};

/*add by liuyihui 2009-08-21*/
/*
*����onenand����֧��
*/
/* OneNAND Controller */
/* OneNAND flash controller */
//#define S3C64XX_PA_ONENAND	   	(0x70100000)
//#define S3C64XX_SZ_ONENAND	   	SZ_1M

static struct resource s3c_onenand_resource[] = {
	[0] = {
		.start = S3C64XX_PA_ONENAND,
		.end   = S3C64XX_PA_ONENAND + S3C64XX_SZ_ONENAND - 1,
		.flags = IORESOURCE_MEM,
	}
};

struct platform_device s3c_device_onenand = {
	.name		  = "onenand",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_onenand_resource),
	.resource	  = s3c_onenand_resource,
	//.dev.platform_data = &s3c_nand_mtd_part_info
};
/*add end*/

EXPORT_SYMBOL(s3c_device_nand);
EXPORT_SYMBOL(s3c_device_onenand);

/* USB Device (Gadget)*/

static struct resource s3c_usbgadget_resource[] = {
	[0] = {
		.start = S3C_PA_OTG,
		.end   = S3C_PA_OTG + S3C_SZ_OTG - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_OTG,
		.end   = IRQ_OTG,
		.flags = IORESOURCE_IRQ,
	}
};

struct platform_device s3c_device_usbgadget = {
	.name		  = "s3c6410-usbgadget",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_usbgadget_resource),
	.resource	  = s3c_usbgadget_resource,
};

EXPORT_SYMBOL(s3c_device_usbgadget);

/* LCD Controller */

static struct resource s3c_lcd_resource[] = {
	[0] = {
		.start = S3C64XX_PA_LCD,
		.end   = S3C64XX_PA_LCD + SZ_1M - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_LCD_VSYNC,
		.end   = IRQ_LCD_SYSTEM,
		.flags = IORESOURCE_IRQ,
	}
};

static u64 s3c_device_lcd_dmamask = 0xffffffffUL;

struct platform_device s3c_device_lcd = {
	.name		  = "s3c-lcd",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_lcd_resource),
	.resource	  = s3c_lcd_resource,
	.dev              = {
		.dma_mask		= &s3c_device_lcd_dmamask,
		.coherent_dma_mask	= 0xffffffffUL
	}
};

/* ADC */
static struct resource s3c_adc_resource[] = {
	[0] = {
		.start = S3C_PA_ADC,
		.end   = S3C_PA_ADC + SZ_4K - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_PENDN,
		.end   = IRQ_PENDN,
		.flags = IORESOURCE_IRQ,
	},
	[2] = {
		.start = IRQ_ADC,
		.end   = IRQ_ADC,
		.flags = IORESOURCE_IRQ,
	}

};

struct platform_device s3c_device_adc = {
	.name		  = "s3c-adc",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_adc_resource),
	.resource	  = s3c_adc_resource,
};

void __init s3c_adc_set_platdata(struct s3c_adc_mach_info *pd)
{
	struct s3c_adc_mach_info *npd;

	npd = kmalloc(sizeof(*npd), GFP_KERNEL);
	if (npd) {
		memcpy(npd, pd, sizeof(*npd));
		s3c_device_adc.dev.platform_data = npd;
	} else {
		printk(KERN_ERR "no memory for ADC platform data\n");
	}
}
EXPORT_SYMBOL(s3c_device_adc);
/* Keypad interface */
static struct resource s3c_keypad_resource[] = {
	[0] = {
		.start = S3C_PA_KEYPAD,
		.end   = S3C_PA_KEYPAD+ S3C_SZ_KEYPAD - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_KEYPAD,
		.end   = IRQ_KEYPAD,
		.flags = IORESOURCE_IRQ,
	}
};

struct platform_device s3c_device_keypad = {
	.name		  = "s3c-keypad",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_keypad_resource),
	.resource	  = s3c_keypad_resource,
};
EXPORT_SYMBOL(s3c_device_keypad);

/*
 * Meizu M8 GPIO Keys,add by liuyihui 09-08-31
 */

#define GPIO_MEIZU_KEY_HOME		148		//S3C64XX_GPN(4)
#define GPIO_MEIZU_KEY_VOL_UP 		146		//S3C64XX_GPN(2)
#define GPIO_MEIZU_KEY_VOL_DOWN	145		//S3C64XX_GPN(1)
#define GPIO_MEIZU_KEY_PLAY 		149		//S3C64XX_GPN(5)
#define GPIO_MEIZU_KEY_POWER		153		//S3C64XX_GPN(9)

static struct gpio_keys_button meizu_m8_button_table[] = {
	{KEY_BACK,			GPIO_MEIZU_KEY_HOME,		1,	"Home button",	EV_KEY,	0,	20},
	{KEY_MENU,			GPIO_MEIZU_KEY_PLAY,    	1,	"Play button",	EV_KEY,	0,	20},
	{KEY_VOLUMEUP,		GPIO_MEIZU_KEY_VOL_UP,   	1,	"Volume up",	EV_KEY,	0,	20},
	{KEY_VOLUMEDOWN,	GPIO_MEIZU_KEY_VOL_DOWN, 	1,	"Volume down",	EV_KEY,	0,	20},
	{KEY_END,			GPIO_MEIZU_KEY_POWER,		1,	"power",		EV_KEY,	1,	20}
};

static struct gpio_keys_platform_data meizum8_button_data = {
	.buttons  = meizu_m8_button_table,
	.nbuttons = ARRAY_SIZE(meizu_m8_button_table),
};

struct platform_device meizu_m8_buttons = {
	.name = "meizum8-buttons",
	.dev  = {
		.platform_data = &meizum8_button_data,
	},
	.id   = -1,
};
EXPORT_SYMBOL(meizu_m8_buttons);
/*
 * End Meizu M8 GPIO Keys
 */


//add by lih 09-10-08
//begin 
struct platform_device s3c_device_accelerometer = {
	.name = "lis302dltr",
	.id   = -1,
};
EXPORT_SYMBOL(s3c_device_accelerometer);
//End 


/* 2D interface */
static struct resource s3c_2d_resource[] = {
	[0] = {
		.start = S3C64XX_PA_2D,
		.end   = S3C64XX_PA_2D + S3C_SZ_2D - 1,
		.flags = IORESOURCE_MEM,
		},
	[1] = {
                .start = IRQ_2D,
                .end   = IRQ_2D,
                .flags = IORESOURCE_IRQ,
        }
};

struct platform_device s3c_device_2d = {
        .name             = "s3c-g2d",
        .id               = -1,
        .num_resources    = ARRAY_SIZE(s3c_2d_resource),
        .resource         = s3c_2d_resource
};

EXPORT_SYMBOL(s3c_device_2d);

/* rotator interface */
static struct resource s3c_rotator_resource[] = {
	[0] = {
		.start = S3C64XX_PA_ROTATOR,
		.end   = S3C64XX_PA_ROTATOR + S3C_SZ_ROTATOR - 1,
		.flags = IORESOURCE_MEM,
		},
	[1] = {
                .start = IRQ_ROTATOR,
                .end   = IRQ_ROTATOR,
                .flags = IORESOURCE_IRQ,
        }
};

struct platform_device s3c_device_rotator = {
        .name             = "s3c-rotator",
        .id               = -1,
        .num_resources    = ARRAY_SIZE(s3c_rotator_resource),
        .resource         = s3c_rotator_resource
};

EXPORT_SYMBOL(s3c_device_rotator);

/* TV encoder */
static struct resource s3c_tvenc_resource[] = {
	[0] = {
		.start = S3C64XX_PA_TVENC,
		.end   = S3C64XX_PA_TVENC + S3C_SZ_TVENC - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_TVENC,
		.end   = IRQ_TVENC,
		.flags = IORESOURCE_IRQ,
	}

};

struct platform_device s3c_device_tvenc = {
	.name		  = "s3c-tvenc",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_tvenc_resource),
	.resource	  = s3c_tvenc_resource,
};

EXPORT_SYMBOL(s3c_device_tvenc);

/* TV scaler */
static struct resource s3c_tvscaler_resource[] = {
	[0] = {
		.start = S3C64XX_PA_TVSCALER,
		.end   = S3C64XX_PA_TVSCALER + S3C_SZ_TVSCALER - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_SCALER,
		.end   = IRQ_SCALER,
		.flags = IORESOURCE_IRQ,
	}

};

struct platform_device s3c_device_tvscaler = {
	.name		  = "s3c-tvscaler",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_tvscaler_resource),
	.resource	  = s3c_tvscaler_resource,
};

EXPORT_SYMBOL(s3c_device_tvscaler);

/* SPI (0) */
static struct resource s3c_spi0_resource[] = {
	[0] = {
		.start = S3C_PA_SPI0,
		.end   = S3C_PA_SPI0 + S3C_SZ_SPI0 - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_SPI0,
		.end   = IRQ_SPI0,
		.flags = IORESOURCE_IRQ,
	}

};

static u64 s3c_device_spi0_dmamask = 0xffffffffUL;

struct platform_device s3c_device_spi0 = {
	.name		  = "sam-spi",
	.id		  = 0,
	.num_resources	  = ARRAY_SIZE(s3c_spi0_resource),
	.resource	  = s3c_spi0_resource,
	.dev              = {
		.dma_mask = &s3c_device_spi0_dmamask,
		.coherent_dma_mask = 0xffffffffUL
	}
};
EXPORT_SYMBOL(s3c_device_spi0);

/* SPI (1) */
static struct resource s3c_spi1_resource[] = {
	[0] = {
		.start = S3C_PA_SPI1,
		.end   = S3C_PA_SPI1 + S3C_SZ_SPI1 - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_SPI1,
		.end   = IRQ_SPI1,
		.flags = IORESOURCE_IRQ,
	}

};

static u64 s3c_device_spi1_dmamask = 0xffffffffUL;

struct platform_device s3c_device_spi1 = {
	.name		  = "sam-spi",
	.id		  = 1,
	.num_resources	  = ARRAY_SIZE(s3c_spi1_resource),
	.resource	  = s3c_spi1_resource,
	.dev              = {
		.dma_mask = &s3c_device_spi1_dmamask,
		.coherent_dma_mask = 0xffffffffUL
	}
};
EXPORT_SYMBOL(s3c_device_spi1);

/* JPEG controller */
static struct resource s3c_jpeg_resource[] = {
	[0] = {
		.start = S3C64XX_PA_JPEG,
		.end   = S3C64XX_PA_JPEG + S3C_SZ_JPEG - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_JPEG,
		.end   = IRQ_JPEG,
		.flags = IORESOURCE_IRQ,
	}

};

struct platform_device s3c_device_jpeg = {
	.name		  = "s3c-jpeg",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_jpeg_resource),
	.resource	  = s3c_jpeg_resource,
};

EXPORT_SYMBOL(s3c_device_jpeg);

/* MFC controller */
static struct resource s3c_mfc_resource[] = {
	[0] = {
		.start = S3C64XX_PA_MFC,
		.end   = S3C64XX_PA_MFC + SZ_4K - 1,
		.flags = IORESOURCE_MEM,
		},
	[1] = {
                .start = IRQ_MFC,
                .end   = IRQ_MFC,
                .flags = IORESOURCE_IRQ,
        }
};

struct platform_device s3c_device_mfc = {
        .name             = "s3c-mfc",
        .id               = -1,
        .num_resources    = ARRAY_SIZE(s3c_mfc_resource),
        .resource         = s3c_mfc_resource
};

EXPORT_SYMBOL(s3c_device_mfc);

/* VPP controller */
static struct resource s3c_vpp_resource[] = {
	[0] = {
		.start = S3C64XX_PA_VPP,
		.end   = S3C64XX_PA_VPP + S3C_SZ_VPP - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_POST0,
		.end   = IRQ_POST0,
		.flags = IORESOURCE_IRQ,
	}

};

struct platform_device s3c_device_vpp = {
	.name		  = "s3c-vpp",
	.id		  = -1,
	.num_resources	  = ARRAY_SIZE(s3c_vpp_resource),
	.resource	  = s3c_vpp_resource,
};

EXPORT_SYMBOL(s3c_device_vpp);

/* 3D interface */
static struct resource s3c_g3d_resource[] = {
	[0] = {
		.start = S3C6410_PA_G3D,
		.end   = S3C6410_PA_G3D + S3C6410_SZ_G3D - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_S3C6410_G3D,
		.end   = IRQ_S3C6410_G3D,
		.flags = IORESOURCE_IRQ,
	}
};

struct platform_device s3c_device_g3d = {
	.name             = "s3c-g3d",
	.id               = -1,
	.num_resources    = ARRAY_SIZE(s3c_g3d_resource),
	.resource         = s3c_g3d_resource
};

EXPORT_SYMBOL(s3c_device_g3d);

#if defined (CONFIG_RTC_DRV_S3C6410)
static struct resource s3c_rtc_resources[] = {
	[0] = {
		.start = S3C64XX_PA_RTC,
		.end	= S3C64XX_PA_RTC + 0x60,
		.flags = IORESOURCE_MEM,
		},
	[1] = {
		.start = IRQ_RTC_ALARM,
		.end	= IRQ_RTC_ALARM,
		.flags  = IORESOURCE_IRQ,
		},
	[2] = {
		.start = IRQ_RTC_TIC,
		.end 	= IRQ_RTC_TIC,
		.flags = IORESOURCE_IRQ,
		}
	};
struct platform_device s3c_device_rtc = {
	.name = "s3c-rtc",
	.id = -1,
	.num_resources = ARRAY_SIZE(s3c_rtc_resources),
	.resource = s3c_rtc_resources,
	};
EXPORT_SYMBOL(s3c_device_rtc);
#endif
