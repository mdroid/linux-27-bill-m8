/*
 * smdk6400_wm8753.c  --  SoC audio for Neo1973
 *
 * Copyright 2007 Wolfson Microelectronics PLC.
 * Author: Graeme Gregory
 *         graeme.gregory@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  Copyright (C) 2007, Ryu Euiyoul <ryu.real@gmail.com>
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  Revision history
 *    20th Jan 2007   Initial version.
 *    05th Feb 2007   Rename all to Neo1973
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>

#include <asm/mach-types.h>

#include <plat/regs-iis.h>
#include <plat/map-base.h>

#include <asm/gpio.h>
#include <plat/gpio-cfg.h>
#include <plat/regs-gpio.h>
#include <plat/gpio-bank-h.h>
#include <plat/gpio-bank-c.h>

#include <mach/hardware.h>
#include <mach/audio.h>
#include <mach/map.h>
#include <asm/io.h>
#include <plat/regs-clock.h>

#include "../codecs/wm8753.h"
#include "s3c-pcm.h"
#include "s3c-i2s.h"

/* define the scenarios */
#define SMDK6400_AUDIO_OFF		0
#define SMDK6400_CAPTURE_MIC1		3
#define SMDK6400_STEREO_TO_HEADPHONES	2
#define SMDK6400_CAPTURE_LINE_IN	1

//#define CONFIG_SND_DEBUG
#ifdef CONFIG_SND_DEBUG
#define s3cdbg(x...) printk(x)
#else
#define s3cdbg(x...)
#endif

static int smdk6400_hifi_hw_params(struct snd_pcm_substream *substream,
	struct snd_pcm_hw_params *params)
{
#if 0
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	unsigned int pll_out = 0;
	int ret = 0;
	unsigned int iispsr, iismod;
	unsigned int prescaler = 4;
	unsigned int bfs, rfs;
	unsigned int*	iis_regs;

	s3cdbg("Entered %s\n", __FUNCTION__);

	iis_regs = ioremap(S3C_IIS_PABASE, 0x100);

	/*PCLK & SCLK gating enable*/
#if defined CONFIG_SND_S3C64XX_I2S_BUS0
	writel(readl(S3C_PCLK_GATE)|S3C_CLKCON_PCLK_IIS0, S3C_PCLK_GATE);
	writel(readl(S3C_SCLK_GATE)|S3C_CLKCON_SCLK_AUDIO0, S3C_SCLK_GATE);
#elif defined CONFIG_SND_S3C64XX_I2S_BUS1
	writel(readl(S3C_PCLK_GATE)|S3C_CLKCON_PCLK_IIS1, S3C_PCLK_GATE);
	writel(readl(S3C_SCLK_GATE)|S3C_CLKCON_SCLK_AUDIO1, S3C_SCLK_GATE);
#endif

	iismod = readl(iis_regs + S3C_IISMOD);
	iismod &=~S3C_IISMOD_RFSMASK;

	/*Clear I2S prescaler value [13:8] and disable prescaler*/
	iispsr = readl(iis_regs + S3C_IISPSR);
	iispsr &=~(S3C_IISPSR_PSVALA|S3C_IISPSR_PSRAEN); //Pre-scaler (Clock divider) inactive
	writel(iispsr, (iis_regs + S3C_IISPSR));

	s3cdbg("%s: %d , params = %d \n", __FUNCTION__, __LINE__, params_rate(params));
	
	switch (params_rate(params)) {
	case 8000:
	case 16000:
	case 32000:
	case 64100:
		/* KDIV=50332, MDIV=32, PDIV=1, SDIV=3 -- Fout=49.152 */
		writel(50332, S3C_EPLL_CON1);
		writel((1<<31)|(32<<16)|(1<<8)|(3<<0) ,S3C_EPLL_CON0);
		break;
	case 11025:
	case 22050:
	case 44100:
	case 88200:
		/* KDIV=10398, MDIV=45, PDIV=1, SDIV=3 -- Fout=67.738 */
		writel(10398, S3C_EPLL_CON1);
		writel((1<<31)|(45<<16)|(1<<8)|(3<<0) ,S3C_EPLL_CON0);
		break;
	case 12000:
	case 24000:
	case 48000:
	case 96000:
		/* KDIV=9961, MDIV=49, PDIV=1, SDIV=3 -- Fin=12, Fout=73.728 */
		writel(9961, S3C_EPLL_CON1);
		writel((1<<31)|(49<<16)|(1<<8)|(3<<0) ,S3C_EPLL_CON0);
		break;
	default:
		writel(0, S3C_EPLL_CON1);
		writel((1<<31)|(128<<16)|(25<<8)|(0<<0) ,S3C_EPLL_CON0);
		break;
	}

	s3cdbg("%s, IISCON: %x IISMOD: %x,IISFIC: %x,IISPSR: %x\n",
			__FUNCTION__ , readl(iis_regs + S3C_IISCON), readl(iis_regs + S3C_IISMOD),
			readl(iis_regs + S3C_IISFIC), readl(iis_regs + S3C_IISPSR));

	while(!(__raw_readl(S3C_EPLL_CON0)&(1<<30)));

	/* MUXepll : FOUTepll */
	writel(readl(S3C_CLK_SRC)|S3C_CLKSRC_EPLL_CLKSEL, S3C_CLK_SRC);

	/* AUDIO0 sel : FOUTepll */
	writel((readl(S3C_CLK_SRC)&~(0x7<<7))|(0<<7), S3C_CLK_SRC);

	/* CLK_DIV2 setting */
	writel(0x0,S3C_CLK_DIV2);

	switch (params_rate(params)) {
	case 8000:
		/*800*256fs=2048000*/
		/* Fout=49.152 */
		pll_out = 2048000;
		prescaler = 8;
		break;
	case 11025:
		/*11025*256fs=2822400*/
		/* Fout=67.738 */
		pll_out = 2822400;
		prescaler = 8;
		break;
	case 12000:
		/*12000*256fs=3072000*/
		/* Fout=73.728 */
		pll_out = 3072000;
		prescaler = 8;
		break;
	case 16000:
		/*16000*256fs=4096000*/
		/* Fout=49.152 */
		pll_out = 4096000;
		prescaler = 4;
		break;
	case 22050:
		/*22050*256fs=5644800*/
		/* Fout=67.738 */
		pll_out = 5644800;
		prescaler = 4;
		break;
	case 24000:
		/*24000*256fs=6144000*/
		/* Fout=73.728 */
		pll_out = 6144000;
		prescaler = 4;
		break;
	case 32000:
		/*32000*256fs=8192000*/
		/* Fout=49.152  */
		pll_out = 8192000;
		prescaler = 2;
		break;
	case 44100:
		/*44100*256fs=11289600*/
		/* Fout=67.738 */
		pll_out = 11289600;
		prescaler = 2;
		break;
	case 48000:
		/*48000*256fs=12288000*/
		/* Fout=73.728 */
		pll_out = 12288000;
		prescaler = 2;
		break;
	case 88200:
		/*88200*256fs=22579200*/
		/* Fout=67.738 */
		pll_out = 22579200;
		prescaler = 1;
		break;
	case 96000:
		/*96000*256fs=24576000*/
		/* Fout=73.728 */
		pll_out = 24576000;
		prescaler = 1;
		break;
	default:
		/* somtimes 32000 rate comes to 96000
		   default values are same as 32000 */
		prescaler = 4;
		pll_out = 12288000;
		break;
	}
	s3cdbg("pll_out =%d prescaler=%d\n",pll_out,prescaler);
	
	s3cdbg("%s: %d , params = ", __FUNCTION__, __LINE__);
	
	/* set MCLK division for sample rate */
	switch (params_format(params)) {
	case SNDRV_PCM_FORMAT_S8:
		s3cdbg("SNDRV_PCM_FORMAT_S8\n");
		prescaler *= 3;
		bfs = 16;
		rfs = 256;		/* Can take any RFS value for AP */
		break;
	case SNDRV_PCM_FORMAT_S16_LE:
		s3cdbg("SNDRV_PCM_FORMAT_S16_LE\n");
		prescaler *= 3;
		bfs = 32;
		rfs = 256;		/* Can take any RFS value for AP */
		break;
	case SNDRV_PCM_FORMAT_S24_3LE:
		s3cdbg("SNDRV_PCM_FORMAT_S24_3LE\n");
		prescaler *= 2;
		bfs = 48;
		rfs = 512;		/* B'coz 48-BFS needs atleast 512-RFS acc to *S5P6440* UserManual */
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
		s3cdbg("SNDRV_PCM_FORMAT_S24_3LE\n");
		prescaler *= 2;
		bfs = 48;
		rfs = 512;		/* B'coz 48-BFS needs atleast 512-RFS acc to *S5P6440* UserManual */
		break;
	default:
		s3cdbg("Unknow format!!\n");
		return -EINVAL;
	}

	prescaler = prescaler - 1;

	/* set codec DAI configuration, codec is slave mode,i2s interface*/
	ret = snd_soc_dai_set_fmt(codec_dai,
		SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
		SND_SOC_DAIFMT_CBS_CFS);
	if (ret < 0){
		printk("%s: %d , excute wm8753_mode1h_set_dai_fmt failure\n", __FUNCTION__, __LINE__);
		return ret;
	}

	/* set cpu DAI configuration */
	ret = snd_soc_dai_set_fmt(cpu_dai,
		SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
		SND_SOC_DAIFMT_CBS_CFS);
	if (ret < 0){
		printk("%s: %d , excute s3c_i2s_set_fmt failure\n", __FUNCTION__, __LINE__);
		return ret;
	}

	/* set the codec system clock for DAC and ADC */
	ret = snd_soc_dai_set_sysclk(codec_dai,WM8753_MCLK,pll_out,
		SND_SOC_CLOCK_IN);
	if (ret < 0){
		printk("%s: %d , excute wm8753_set_dai_sysclk failure\n", __FUNCTION__, __LINE__);
		return ret;
	}

	ret = snd_soc_dai_set_clkdiv(cpu_dai, S3C_DIV_MCLK, rfs);
	if (ret < 0){
		printk("%s: %d , excute s3c_i2s_set_clkdiv  failure\n", __FUNCTION__, __LINE__);
		return ret;
	}

	ret = snd_soc_dai_set_clkdiv(cpu_dai, S3C_DIV_BCLK, bfs);
	if (ret < 0){
		printk("%s: %d , excute s3c_i2s_set_clkdiv  failure\n", __FUNCTION__, __LINE__);
		return ret;
	}
	
	/* set VXDOSR, ADCOSR and DACOSR to 1 for saving power*/
	ret = snd_soc_dai_set_clkdiv(codec_dai, WM8753_OSRCLR, 0x0);
	if (ret < 0){
		printk("%s: %d , excute wm8753_set_dai_clkdiv  failure\n", __FUNCTION__, __LINE__);
		return ret;
	}

	/* set prescaler division for sample rate */
	ret = snd_soc_dai_set_clkdiv(cpu_dai, S3C_DIV_PRESCALER,prescaler);
	if (ret < 0){
		printk("%s: %d , excute s3c_i2s_set_clkdiv  failure\n", __FUNCTION__, __LINE__);
		return ret;
	}
	
#if 1
	ret = snd_soc_dai_set_sysclk(cpu_dai,S3C_CLKSRC_CLKAUDIO,params_rate(params),
		SND_SOC_CLOCK_OUT);
	if (ret < 0){
		printk("%s: %d , excute s3c_i2s_set_sysclk failure\n", __FUNCTION__, __LINE__);
		return ret;
	}
#endif	
	return 0;
#else
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;
	int bfs, rfs, psr, ret;
	unsigned int src_clk = s3c_i2s_get_clockrate();
	unsigned int param_fmt = params_format(params);
	unsigned int param_rate = params_rate(params);
	
	s3cdbg("%s\n", __FUNCTION__);

	/* Choose BFS and RFS values combination that is supported by
	 * both the WM8753 codec as well as the S3C AP
	 *
	 * WM8753 codec supports only S16_LE, S20_3LE, S24_LE & S32_LE.
	 * S3C AP supports only S8, S16_LE & S24_LE.
	 * We implement all for completeness but only S16_LE & S24_LE bit-lengths 
	 * are possible for this AP-Codec combination.
	 */
	switch (param_fmt) {
	case SNDRV_PCM_FORMAT_S8:
		bfs = 16;
		rfs = 256;		/* Can take any RFS value for AP */
 		break;
 	case SNDRV_PCM_FORMAT_S16_LE:
		bfs = 32;
		rfs = 256;		/* Can take any RFS value for AP */
 		break;
	case SNDRV_PCM_FORMAT_S20_3LE:
 	case SNDRV_PCM_FORMAT_S24_LE:
		bfs = 48;
		rfs = 512;		/* B'coz 48-BFS needs atleast 512-RFS acc to *S5P6440* UserManual */
 		break;
 	case SNDRV_PCM_FORMAT_S32_LE:	/* Impossible, as the AP doesn't support 64fs or more BFS */
	default:
		return -EINVAL;
 	}
 
	/* Select the AP Sysclk */
	ret = snd_soc_dai_set_sysclk(cpu_dai, S3C_CDCLKSRC_INT,param_rate, SND_SOC_CLOCK_OUT);
	if (ret < 0)
		return ret;


#if USE_AP_MASTER

#ifdef USE_CLKAUDIO
	ret = snd_soc_dai_set_sysclk(cpu_dai, S3C_CLKSRC_CLKAUDIO,param_rate, SND_SOC_CLOCK_OUT);
#else
	ret = snd_soc_dai_set_sysclk(cpu_dai, S3C_CLKSRC_PCLK, 0, SND_SOC_CLOCK_OUT);
#endif

#else
	s3cdbg("CLKSRC:SLVPCLK\n");
	ret = snd_soc_dai_set_sysclk(cpu_dai, S3C_IISMOD_SLVPCLK, 0, SND_SOC_CLOCK_OUT);
#endif

	if (ret < 0)
		return ret;

	/* Set the AP DAI configuration */
#if USE_AP_MASTER
	ret = snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
#else
	ret = snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);
#endif

	if (ret < 0)
		return ret;

	/* Set the AP RFS */
	ret = snd_soc_dai_set_clkdiv(cpu_dai, S3C_DIV_MCLK, rfs);
	if (ret < 0)
		return ret;

	/* Set the AP BFS */
	ret = snd_soc_dai_set_clkdiv(cpu_dai, S3C_DIV_BCLK, bfs);
	if (ret < 0)
		return ret;

	switch (params_rate(params)) {
	case 8000:
	case 11025:
	case 16000:
	case 22050:
	case 32000:
	case 44100: 
	case 48000:
	case 64000:
	case 88200:
	case 96000:
		psr = src_clk / rfs / param_rate;
		ret = src_clk / rfs - psr * param_rate;
		if(ret >= param_rate/2)	// round off
		   psr += 1;
		psr -= 1;
		break;
	default:
		return -EINVAL;
	}

	s3cdbg("SRC_CLK=%d PSR=%d RFS=%d BFS=%d\n", src_clk, psr, rfs, bfs);

	/* Set the AP Prescalar */
	ret = snd_soc_dai_set_clkdiv(cpu_dai, S3C_DIV_PRESCALER, psr);
	if (ret < 0)
		return ret;

	/* Set the Codec DAI configuration */
#if USE_AP_MASTER
	ret = snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
#else
	ret = snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);
#endif
	if (ret < 0)
		return ret;

#if USE_AP_MASTER
	ret = snd_soc_dai_set_sysclk(codec_dai,WM8753_MCLK,src_clk,SND_SOC_CLOCK_IN);
#else
	ret = snd_soc_dai_set_sysclk(codec_dai,WM8753_MCLK,src_clk,SND_SOC_CLOCK_OUT);
#endif
	if (ret < 0)
		return ret;

	/* set VXDOSR, ADCOSR and DACOSR to 1 for saving power*/
	ret = snd_soc_dai_set_clkdiv(codec_dai, WM8753_OSRCLR, 0x00);
	if (ret < 0){
		printk("%s: %d , excute wm8753_set_dai_clkdiv  failure\n", __FUNCTION__, __LINE__);
		return ret;
	}
	return 0;
	
#endif
}

static int smdk6400_hifi_hw_free(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;

	/* disable the PLL */
	return codec_dai->dai_ops.set_pll(codec_dai, WM8753_PLL1, 0, 0);
}

/*
 * Neo1973 WM8753 HiFi DAI opserations.
 */
static struct snd_soc_ops smdk6400_hifi_ops = {
	.hw_params = smdk6400_hifi_hw_params,
	.hw_free = smdk6400_hifi_hw_free,
};

static int smdk6400_scenario = 0;

static int smdk6400_get_scenario(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = smdk6400_scenario;
	return 0;
}

static int set_scenario_endpoints(struct snd_soc_codec *codec, int scenario)
{
	switch(smdk6400_scenario) {
	case SMDK6400_AUDIO_OFF:
		snd_soc_dapm_set_endpoint(codec, "Headphone Jack",    0);
		snd_soc_dapm_set_endpoint(codec, "Mic1 Jack",  0);
		snd_soc_dapm_set_endpoint(codec, "Line In Jack",  0);
		break;
	case SMDK6400_STEREO_TO_HEADPHONES:
		snd_soc_dapm_set_endpoint(codec, "Headphone Jack",    1);
		snd_soc_dapm_set_endpoint(codec, "Mic1 Jack",  0);
		snd_soc_dapm_set_endpoint(codec, "Line In Jack",  0);
		break;
	case SMDK6400_CAPTURE_MIC1:
		snd_soc_dapm_set_endpoint(codec, "Headphone Jack",    0);
		snd_soc_dapm_set_endpoint(codec, "Mic1 Jack",  1);
		snd_soc_dapm_set_endpoint(codec, "Line In Jack",  0);
		break;
	case SMDK6400_CAPTURE_LINE_IN:
		snd_soc_dapm_set_endpoint(codec, "Headphone Jack",    0);
		snd_soc_dapm_set_endpoint(codec, "Mic1 Jack",  0);
		snd_soc_dapm_set_endpoint(codec, "Line In Jack",  1);
		break;
	default:
		snd_soc_dapm_set_endpoint(codec, "Headphone Jack",    1);
		snd_soc_dapm_set_endpoint(codec, "Mic1 Jack",  1);
		snd_soc_dapm_set_endpoint(codec, "Line In Jack",  1);
		break;
	}

	snd_soc_dapm_sync_endpoints(codec);

	return 0;
}

static int smdk6400_set_scenario(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);

	if (smdk6400_scenario == ucontrol->value.integer.value[0])
		return 0;

	smdk6400_scenario = ucontrol->value.integer.value[0];
	set_scenario_endpoints(codec, smdk6400_scenario);
	return 1;
}

static const struct snd_soc_dapm_widget wm8753_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphone Jack", NULL),
	SND_SOC_DAPM_MIC("Mic1 Jack", NULL),
	SND_SOC_DAPM_LINE("Line In Jack", NULL),
};


/* example machine audio_mapnections */
//static const char* audio_map[][3] = {
static const struct snd_soc_dapm_route audio_map[]={
	{"Headphone Jack", NULL, "LOUT1"},
	{"Headphone Jack", NULL, "ROUT1"},

	/* mic is connected to mic1 - with bias */
	/* mic is connected to mic1 - with bias */
	{"MIC1", NULL, "Mic1 Jack"},

	{"LINE1", NULL, "Line In Jack"},
	{"LINE2", NULL, "Line In Jack"},

	/* Connect the ALC pins */
	{"ACIN", NULL, "ACOP"},

	//{NULL, NULL, NULL},
};

static const char *smdk_scenarios[] = {
	"Off",
	"Capture Line In",
	"Headphones",
	"Capture Mic1",
};

static const struct soc_enum smdk_scenario_enum[] = {
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(smdk_scenarios),smdk_scenarios),
};

static const struct snd_kcontrol_new wm8753_smdk6400_controls[] = {
	SOC_ENUM_EXT("SMDK Mode", smdk_scenario_enum[0],
		smdk6400_get_scenario, smdk6400_set_scenario),
};

/*
 * This is an example machine initialisation for a wm8753 connected to a
 * smdk6400. It is missing logic to detect hp/mic insertions and logic
 * to re-route the audio in such an event.
 */
static int smdk6400_wm8753_init(struct snd_soc_codec *codec)
{
	int i, err;

	/* set endpoints to default mode */
	set_scenario_endpoints(codec, SMDK6400_AUDIO_OFF);

	/* Add smdk6400 specific widgets */
	for (i = 0; i < ARRAY_SIZE(wm8753_dapm_widgets); i++)
		snd_soc_dapm_new_control(codec, &wm8753_dapm_widgets[i]);

	/* add smdk6400 specific controls */
	for (i = 0; i < ARRAY_SIZE(wm8753_smdk6400_controls); i++) {
		err = snd_ctl_add(codec->card,
				snd_soc_cnew(&wm8753_smdk6400_controls[i],
				codec, NULL));
		if (err < 0)
			return err;
	}

	/* set up smdk6400 specific audio path audio_mapnects */
	err = snd_soc_dapm_add_routes(codec, audio_map,
				      ARRAY_SIZE(audio_map));

	/* always connected */
	snd_soc_dapm_set_endpoint(codec, "Mic1 Jack", 1);
	snd_soc_dapm_set_endpoint(codec, "Headphone Jack", 1);
	snd_soc_dapm_set_endpoint(codec, "Line In Jack", 1);

	snd_soc_dapm_sync_endpoints(codec);
	return 0;
}

static struct snd_soc_dai_link smdk6400_dai[] = {
{ /* Hifi Playback - for similatious use with voice below */
	.name = "WM8753",
	.stream_name = "WM8753 HiFi",
	.cpu_dai = &s3c_i2s_dai,
	.codec_dai = &wm8753_dai[WM8753_DAI_HIFI],
	.init = smdk6400_wm8753_init,
	.ops = &smdk6400_hifi_ops,
},
};

static struct snd_soc_machine smdk6400 = {
	.name = "smdk6400",
	.dai_link = smdk6400_dai,
	.num_links = ARRAY_SIZE(smdk6400_dai),
};

static struct wm8753_setup_data smdk6400_wm8753_setup = {
	.i2c_address = 0x1a,
};

static struct snd_soc_device smdk6400_snd_devdata = {
	.machine = &smdk6400,
	.platform = &s3c24xx_soc_platform,
	.codec_dev = &soc_codec_dev_wm8753,
	.codec_data = &smdk6400_wm8753_setup,
};

static struct platform_device *smdk6400_snd_device;

static int __init smdk6400_init(void)
{
	int ret;

	smdk6400_snd_device = platform_device_alloc("soc-audio", -1);
	if (!smdk6400_snd_device)
		return -ENOMEM;

	platform_set_drvdata(smdk6400_snd_device, &smdk6400_snd_devdata);
	smdk6400_snd_devdata.dev = &smdk6400_snd_device->dev;
	ret = platform_device_add(smdk6400_snd_device);

	if (ret)
		platform_device_put(smdk6400_snd_device);

	return ret;
}

static void __exit smdk6400_exit(void)
{
	platform_device_unregister(smdk6400_snd_device);
}

module_init(smdk6400_init);
module_exit(smdk6400_exit);

/* Module information */
MODULE_AUTHOR("Ryu Euiyoul");
MODULE_DESCRIPTION("ALSA SoC WM8753 Neo1973");
MODULE_LICENSE("GPL");
